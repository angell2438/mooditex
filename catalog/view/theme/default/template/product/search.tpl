<?php echo $header; ?>
<div class="container">
          <ul  class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
            <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
              <?php if($i+1<count($breadcrumbs)) { ?>
              <a itemscope itemtype="http://schema.org/Thing" itemprop="item" href="<?php echo $breadcrumb['href']; ?>">
                <span itemprop="name"><?php echo $breadcrumb['text']; ?></span></a>
              <?php } else { ?><span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
              <?php } ?>
            </li>
            <?php } ?>
          </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
      <label class="control-label" for="input-search"><?php echo $entry_search; ?></label>
      <div class="row">
        <div class="col-sm-4">
          <input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_keyword; ?>" id="input-search" class="form-control" />
        </div>
        <div class="col-sm-3">
          <select name="category_id" class="form-control">
            <option value="0"><?php echo $text_category; ?></option>
            <?php foreach ($categories as $category_1) { ?>
            <?php if ($category_1['category_id'] == $category_id) { ?>
            <option value="<?php echo $category_1['category_id']; ?>" selected="selected"><?php echo $category_1['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $category_1['category_id']; ?>"><?php echo $category_1['name']; ?></option>
            <?php } ?>
            <?php foreach ($category_1['children'] as $category_2) { ?>
            <?php if ($category_2['category_id'] == $category_id) { ?>
            <option value="<?php echo $category_2['category_id']; ?>" selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_2['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $category_2['category_id']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_2['name']; ?></option>
            <?php } ?>
            <?php foreach ($category_2['children'] as $category_3) { ?>
            <?php if ($category_3['category_id'] == $category_id) { ?>
            <option value="<?php echo $category_3['category_id']; ?>" selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $category_3['category_id']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>
            <?php } ?>
            <?php } ?>
            <?php } ?>
            <?php } ?>
          </select>
        </div>
          <div class="col-sm-6 hidden">
              <label class="checkbox-inline">
                  <?php if ($sub_category) { ?>
                      <input type="checkbox" name="sub_category" value="1" checked="checked" />
                  <?php } else { ?>
                      <input type="checkbox" name="sub_category" value="1" />
                  <?php } ?>
                  <?php echo $text_sub_category; ?></label>
          </div>
          <div class="col-sm-12 ">
              <label class="checkbox-inline form-checkbox">
                  <?php if ($description) { ?>
                  <input type="checkbox" name="description" value="1" id="description" checked="checked" />
                  <span class="form-checkbox__marker"></span>

                  <span class="form-checkbox__label"> <?php echo $entry_description; ?></label></span>
              <?php } else { ?>
                  <input type="checkbox" name="description" value="1" id="description" />
                  <span class="form-checkbox__marker"></span>

                  <span class="form-checkbox__label"> <?php echo $entry_description; ?></label></span>
              <?php } ?>

          </div>
      <input type="button" value="<?php echo $button_search; ?>" id="button-search" class="btn btn-default" />
      <h2><?php echo $text_search; ?></h2>
      <?php if ($products) { ?>
          <div class="row flex-container">
              <div class="col-sm-6 text-left results-text hidden-sm hidden-xs"><?php echo $results; ?></div>
              <div class="col-md-2 text-right">
                  <label class="control-label" for="input-sort"><?php echo $text_sort; ?></label>
              </div>
              <div class="col-md-4 text-right">
                  <select id="input-sort" class="form-control" onchange="location = this.value;">
                      <?php foreach ($sorts as $sorts) { ?>
                          <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                              <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
                          <?php } else { ?>
                              <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
                          <?php } ?>
                      <?php } ?>
                  </select>
              </div>
          </div>
      <div class="row">
          <?php foreach ($products as $product) { ?>
              <div class="product-layout product-list col-md-3 col-sm-4 col-xs-6 col-xxs-12">
                  <div class="product-thumb transition">
                      <div class="image">
                          <a href="<?php echo $product['href']; ?>">
                              <img src="<?php echo $product['thumb']; ?>"
                                   alt="<?php echo $product['name']; ?>"
                                   title="<?php echo $product['name']; ?>"  />
                          </a>
                      </div>
                      <div class="caption">
                          <a href="<?php echo $product['href']; ?>">
                              <?php echo $product['name']; ?>
                              <?php if ($product['price']) { ?>
                                  <p class="price">
                                      <?php if (!$product['special']) { ?>
                                          <span><?php echo $product['price']; ?></span>
                                      <?php } else { ?>
                                          <span class="price-new"><?php echo $product['special']; ?></span>
                                          <span class="price-old"><?php echo $product['price']; ?></span>
                                      <?php } ?>
                                  </p>
                              <?php } ?>
                          </a>

                      </div>
                  </div>
              </div>
          <?php } ?>
      </div>
      <div class="row">
          <div class="col-sm-12"><?php echo $pagination; ?></div>
      </div>
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<script type="text/javascript"><!--
$('#button-search').bind('click', function() {
	url = 'index.php?route=product/search';

	var search = $('#content input[name=\'search\']').prop('value');

	if (search) {
		url += '&search=' + encodeURIComponent(search);
	}

	var category_id = $('#content select[name=\'category_id\']').prop('value');

	if (category_id > 0) {
		url += '&category_id=' + encodeURIComponent(category_id);
	}

	var sub_category = $('#content input[name=\'sub_category\']:checked').prop('value');

	if (sub_category) {
		url += '&sub_category=true';
	}

	var filter_description = $('#content input[name=\'description\']:checked').prop('value');

	if (filter_description) {
		url += '&description=true';
	}

	location = url;
});

$('#content input[name=\'search\']').bind('keydown', function(e) {
	if (e.keyCode == 13) {
		$('#button-search').trigger('click');
	}
});

$('select[name=\'category_id\']').on('change', function() {
	if (this.value == '0') {
		$('input[name=\'sub_category\']').prop('disabled', true);
	} else {
		$('input[name=\'sub_category\']').prop('disabled', false);
	}
});

$('select[name=\'category_id\']').trigger('change');
--></script>
<?php echo $footer; ?>