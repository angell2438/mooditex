<?php echo $header; ?>
<div class="container">
          <ul  class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
            <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
              <?php if($i+1<count($breadcrumbs)) { ?>
              <a itemscope itemtype="http://schema.org/Thing" itemprop="item" href="<?php echo $breadcrumb['href']; ?>">
                <span itemprop="name"><?php echo $breadcrumb['text']; ?></span></a>
              <?php } else { ?><span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
              <?php } ?>
            </li>
            <?php } ?>
          </ul>
  <div class="row">
      <div class="col-md-12">
          <div class="wrapper-category_img">
              <h1><?php echo $heading_title; ?></h1>
              <?php if ($thumb) { ?>

                  <div class="image-items">
                      <img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>" />
                  </div>
              <?php } ?>
          </div>
      </div>
      <?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9 col-xs-12'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">
        <?php echo $content_top; ?>

        <?php if ($thumb || $description) { ?>

        <?php } ?>
      <?php if ($products) { ?>
      <div class="row flex-container">
          <div class="col-sm-6 text-left results-text hidden-sm hidden-xs"><?php echo $results; ?></div>
        <div class="col-md-2 text-right">
          <label class="control-label" for="input-sort"><?php echo $text_sort; ?></label>
        </div>
        <div class="col-md-4 text-right">
          <select id="input-sort" class="form-control" onchange="location = this.value;">
            <?php foreach ($sorts as $sorts) { ?>
            <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
            <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
            <?php } else { ?>
            <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
            <?php } ?>
            <?php } ?>
          </select>
        </div>
      </div>
      <div class="row">
        <?php foreach ($products as $product) { ?>
        <div class="product-layout product-list col-md-4 col-sm-6 col-xs-6 col-xxs-12">
            <div class="product-thumb transition">
                <div class="image">
                    <a href="<?php echo $product['href']; ?>">
                        <img src="<?php echo $product['thumb']; ?>"
                             alt="<?php echo $product['name']; ?>"
                             title="<?php echo $product['name']; ?>"  />
                    </a>
                </div>
                <div class="caption">
                    <a href="<?php echo $product['href']; ?>">
                        <?php echo $product['name']; ?>
                        <?php if ($product['price']) { ?>
                            <p class="price">
                                <?php if (!$product['special']) { ?>
                                    <span><?php echo $product['price']; ?></span>
                                <?php } else { ?>
                                    <span class="price-new"><?php echo $product['special']; ?></span>
                                    <span class="price-old"><?php echo $product['price']; ?></span>
                                <?php } ?>
                            </p>
                        <?php } ?>
                    </a>

                </div>
            </div>
        </div>
        <?php } ?>

      </div>
      <div class="row">
          <hr>
          <?php if ($description) { ?>
                  <div class="col-sm-12"><?php echo $description; ?></div>
          <?php } ?>
        <div class="col-sm-12"><?php echo $pagination; ?></div>

      </div>
      <?php } ?>
      <?php if (!$categories && !$products) { ?>
      <p><?php echo $text_empty; ?></p>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-default"><span><?php echo $button_continue; ?></span></a></div>
      </div>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
