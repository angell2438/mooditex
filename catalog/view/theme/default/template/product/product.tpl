<?php echo $header; ?>
<div class="container" >
    <ul class="breadcrumb"  itemscope itemtype="http://schema.org/BreadcrumbList">
        <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                <?php if($i+1<count($breadcrumbs)) { ?>
                    <a itemscope itemtype="http://schema.org/Thing" itemprop="item" href="<?php echo $breadcrumb['href']; ?>">
                        <span itemprop="name"><?php echo $breadcrumb['text']; ?></span></a>
                <?php } else { ?><span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
                <?php } ?>
            </li>
        <?php } ?>
    </ul>
    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
            <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
            <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
            <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>" itemscope itemtype="http://schema.org/Product"><?php echo $content_top; ?>
            <div class="row">
                <?php if ($column_left || $column_right) { ?>
                    <?php $class = 'col-sm-4'; ?>
                <?php } else { ?>
                    <?php $class = 'col-sm-4'; ?>
                <?php } ?>
                <div class="<?php echo $class; ?>">
                        <?php if ($thumb || $images) { ?>
                                <?php if(count($images) >= 1) { ?>
                                    <div class="slider-image-big-curier">
                                        <div class="thumbnails" id="preview_imgs">
                                            <?php $k = '';
                                            foreach ($images as $image) {
                                                $k++; ?>
                                                <?php if ($image): ?>
                                                    <div class="item ">
                                                        <img src="<?php echo $image['thumb']; ?>"
                                                             data-link="image-<?php echo $k; ?>"
                                                             class="img-responsive"
                                                             title="<?php echo $heading_title; ?>"
                                                             alt="<?php echo $heading_title; ?>"/>
                                                    </div>
                                                <?php endif;  ?>
                                            <?php } ?>
                                        </div>
                                        <button class="click-triger-zoom btn-zoom"><i class="icon-search"></i></button>
                                    </div>
                                    <div class="thumbnails-mini" id="thumbnail_imgs">
                                        <?php $k = '';
                                        foreach ($images as $image) {
                                            $k++; ?>
                                            <?php if ($image): ?>
                                                <div class="item ">
                                                    <img src="<?php echo $image['thumb']; ?>"
                                                         data-link="image-<?php echo $k; ?>"
                                                         class="img-responsive"
                                                         title="<?php echo $heading_title; ?>"
                                                         alt="<?php echo $heading_title; ?>"/>
                                                </div>
                                            <?php endif;  ?>
                                        <?php } ?>
                                    </div>
                                <ul id="lightgallery" style="display: none">
                                    <li data-src="<?php echo $popup; ?>" id="image-0">
                                        <a href="">
                                            <img src="<?php echo $image['thumb']; ?>" alt="<?php echo $heading_title; ?>">
                                        </a>
                                    </li>
                                    <?php $k = '';
                                    foreach ($images as $image) {
                                        $k++; ?>
                                        <?php if ($image['option']): ?>
                                            <li data-src="<?php echo $image['popup']; ?>" id="image-<?php echo $k; ?>">
                                                <a href="">
                                                    <img src="<?php echo $image['popup']; ?>" alt="<?php echo $heading_title; ?>">
                                                </a>
                                            </li>
                                        <?php endif;  ?>

                                    <?php } ?>
                                </ul>
                                <?php } else { ?>
                                    <?php if ($thumb) { ?>
                                        <div class="slider-image-big">
                                            <div class="item ">
                                                <a  class="pop-img" href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>">
                                                    <img itemprop="image" src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" />
                                                </a>
                                            </div>
                                        </div>
                                    <?php } ?>
                                <?php } ?>

                                <?php
                                $images_html = array();
                                foreach ($images as $image):
                                    if (!isset($images_html[$image['option']]))
//                                  var_dump($image['option']);
                                        $images_html[$image['option']] = array();

                                    if(!isset($images_html[$image['option']]['previews'])){
                                        $images_html[$image['option']]['previews'] = '';
                                    }
                                    if(!isset($images_html[$image['option']]['thumbs'])){
                                        $images_html[$image['option']]['thumbs'] = '';
                                    }
                                    $images_html[$image['option']]['previews'] .= '<div class="item ">
                                                                                      <img itemprop="image" src="' . $image['thumb'] . '" alt="' . $heading_title . '">
                                                                             </div>';
                                    $images_html[$image['option']]['thumbs']   .=  '<div class="item ">
                                                                                      <img itemprop="image" src="' . $image['thumb'] . '" alt="' . $heading_title . '">
                                                                             </div>';
                                endforeach;
                                foreach ($images_html as $key => $images_html_item):
                                    ?>
                                    <div class="hidden" id="previews_<?= $key ?>">
                                        <?= $images_html_item['previews'] ?>
                                        <?php $k = '';
                                        foreach ($images as $image) {
                                            $k++; ?>
                                            <?php if ($image): ?>
                                                <div class="item ">
                                                    <img src="<?php echo $image['thumb']; ?>"
                                                         data-link="image-<?php echo $k; ?>"
                                                         class="img-responsive"
                                                         title="<?php echo $heading_title; ?>"
                                                         alt="<?php echo $heading_title; ?>"/>
                                                </div>
                                            <?php endif;  ?>
                                        <?php } ?>
                                    </div>
                                    <div class="hidden" id="thumbs_<?= $key ?>">
                                        <?php $k = '';
                                        foreach ($images as $image) {
                                            $k++; ?>
                                            <?php if ($image == 0): ?>
                                                <div class="item ">
                                                    <img src="<?php echo $image['thumb']; ?>"
                                                         data-link="image-<?php echo $k; ?>"
                                                         class="img-responsive"
                                                         title="<?php echo $heading_title; ?>"
                                                         alt="<?php echo $heading_title; ?>"/>
                                                </div>
                                            <?php endif;  ?>
                                        <?php } ?>
                                    </div>
                                <?php
                                endforeach;
                                ?>


                        <?php } ?>
<!--                    </div>-->
                </div>
                <?php if ($column_left || $column_right) { ?>
                    <?php $class = 'col-sm-8 col-xs-12'; ?>
                <?php } else { ?>
                    <?php $class = 'col-sm-8 col-xs-12'; ?>
                <?php } ?>
                <div class="<?php echo $class; ?>">

                    <div class="product-title">
                        <h1 itemprop="name" ><?php echo $heading_title; ?></h1>
                    </div>
                    <?php if ($price) { ?>
                        <div class="price-wrapper" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                            <meta itemprop="priceCurrency" content="<?php echo $cur_cur_code ?>" />
                            <div class="flex-wrapper">
                                <?php if (!$special) { ?>
                                    <p class="price" itemprop="price" content="<?php echo preg_replace('/\s+/', '', $raw_price); ?>"><?php echo $price; ?></p>
                                <?php } else { ?>
                                    <p class="price" itemprop="price" content="<?php echo preg_replace('/\s+/', '', $raw_special); ?>"><?php echo $special; ?></p>
                                    <span class="price-old"  itemprop="price" content="<?php echo preg_replace('/\s+/', '', $raw_price); ?>"><?php echo $price; ?></span>
                                <?php } ?>
                            </div>

                        </div>
                    <?php } ?>
                    <?php if ($description) { ?>
                        <div class="description_wrapper_mini">
                            <?php echo $description; ?>
                        </div>
                    <?php } ?>
                    <div id="product" class="option-wrapper">
                        <?php if ($options) { ?>
                            <?php foreach ($options as $option) { ?>
                                <?php if ($option['type'] == 'select') { ?>
                                    <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                        <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                        <select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control">
                                            <option value=""><?php echo $text_select; ?></option>
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                                                    <?php if ($option_value['price']) { ?>
                                                        (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                    <?php } ?>
                                                </option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                <?php } ?>
                                <?php if ($option['type'] == 'radio') { ?>
                                    <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                        <label class="control-label"><?php echo $option['name']; ?></label>
                                        <div id="input-option<?php echo $option['product_option_id']; ?>" class="opt_wrapp size_opt">
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                <div class="radio ">
                                                    <input id="option-<?php echo $option_value['product_option_value_id']; ?>" type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                                                    <label for="option-<?php echo $option_value['product_option_value_id']; ?>">
                                                        <?php echo $option_value['name']; ?>
                                                        <?php if ($option_value['price']) { ?>
                                                            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                        <?php } ?>
                                                    </label>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if ($option['type'] == 'checkbox') { ?>
                                    <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                        <label class="control-label"><?php echo $option['name']; ?></label>
                                        <div id="input-option<?php echo $option['product_option_id']; ?>">
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                                                        <?php echo $option_value['name']; ?>
                                                        <?php if ($option_value['price']) { ?>
                                                            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                        <?php } ?>
                                                    </label>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if ($option['type'] == 'image') { ?>
                                    <div class="margin-option form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
<!--                                        <label class="control-label">--><?php //echo $option['name']; ?><!--:</label>-->
                                        <div id="input-option<?php echo $option['product_option_id']; ?>" class="opt_wrapp size_opt">
                                            <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                <div class="radio radio-image">
                                                    <input data-option="<?= $option_value['option_value_id']?>" id="option-<?php echo $option_value['product_option_value_id']; ?>" type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                                                    <label for="option-<?php echo $option_value['product_option_value_id']; ?>">
                                                        <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" />
                                                        <?php if ($option_value['price']) { ?>
                                                            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                        <?php } ?>
                                                    </label>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if ($option['type'] == 'text') { ?>
                                    <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                        <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                        <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                    </div>
                                <?php } ?>
                                <?php if ($option['type'] == 'textarea') { ?>
                                    <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                        <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                        <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"><?php echo $option['value']; ?></textarea>
                                    </div>
                                <?php } ?>
                                <?php if ($option['type'] == 'file') { ?>
                                    <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                        <label class="control-label"><?php echo $option['name']; ?></label>
                                        <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default btn-block"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
                                        <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>" />
                                    </div>
                                <?php } ?>
                                <?php if ($option['type'] == 'date') { ?>
                                    <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                        <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                        <div class="input-group date">
                                            <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                            <span class="input-group-btn">
                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                </span></div>
                                    </div>
                                <?php } ?>
                                <?php if ($option['type'] == 'datetime') { ?>
                                    <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                        <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                        <div class="input-group datetime">
                                            <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                            <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
                                    </div>
                                <?php } ?>
                                <?php if ($option['type'] == 'time') { ?>
                                    <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                        <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                        <div class="input-group time">
                                            <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                            <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                            <script>
                                $('[name=\'option[<?php echo $option["product_option_id"]; ?>]\']').prop('checked', false);
                                $('[name=\'option[<?php echo $option["product_option_id"]; ?>]\']').change(function(){
                                    console.log($(this).data('option'));
                                    var option = $(this).data('option');
                                    if ($('#previews_' + $(this).data('option')).length && $('#thumbs_' + $(this).data('option')).length){
                                        var previews_container = $('#previews_' + option),
                                            thumbs_container = $('#thumbs_' + option);
                                    }
                                    else if ($('#previews_0')  && $('#thumbs_0').length){
                                        var previews_container = $('#previews_0'),
                                            thumbs_container = $('#thumbs_0');
                                    }
                                    else{
                                        var previews_container = false,
                                            thumbs_container = false;
                                    }
                                    if (previews_container && thumbs_container){
                                        $('.thumbnails, .thumbnails-mini').animate({
                                            opacity: 0,
                                            visibility: 'hidden'
                                        }, 200, function() {
                                            $('#preview_imgs').html(previews_container.html());
                                            $('#thumbnail_imgs').html(thumbs_container.html());

                                            $('.thumbnails').removeClass('slick-initialized').slick({
                                                slidesToShow: 1,
                                                slidesToScroll: 1,
                                                arrows: true,
                                                nextArrow: '<i class="icon-arrow-right arrow-next"></i>',
                                                prevArrow: '<i class="icon-arrow-left arrow-prev"></i>',
                                                fade: true,
                                                asNavFor: '.thumbnails-mini'
                                            }).slick('setPosition');
                                            $('.thumbnails-mini').removeClass('slick-initialized').slick({
                                                slidesToShow: 4,
                                                slidesToScroll: 1,
                                                asNavFor: '.thumbnails',
                                                dots: false,
                                                arrows: false,
                                                centerMode: true,
                                                focusOnSelect: true,
                                                centerPadding: '0px',
                                            }).slick('setPosition');
                                            if ($(window).width() > 767) {
                                                $('.thumbnails').on('afterChange', function (event, slick, currentSlide) {
                                                    $(".thumbnails .slick-current img").elevateZoom({
                                                        zoomType: "lens",
                                                        lensShape: "square",
                                                        lensSize: 160,
                                                        borderSize: 0
                                                    });
                                                });
                                                $(".thumbnails .slick-current img").elevateZoom({
                                                    zoomType: "lens",
                                                    lensShape: "square",
                                                    lensSize: 160,
                                                    borderSize: 0
                                                });
                                            }
                                            $('.click-triger-zoom').on('click', function (e) {
                                                $('.thumbnails .slick-current img').data("link");
                                                $('#' + $('.thumbnails .slick-current img').data("link")).trigger("click");
                                            });

                                            setTimeout(function(){
                                                $('.thumbnails, .thumbnails-mini').animate({
                                                    opacity: 1,
                                                    visibility: 'visible'
                                                }, 250);
                                            }, 250);
                                        });
                                    }
                                })
                            </script>
                        <?php } ?>
                        <div class="item-btn_wrapper clearfix">
                            <input type="hidden" name="quantity" value="<?php echo $minimum; ?>"  id="input-quantity" class="form-control" />
                            <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
<!--                            <button type="button" id="button-cart" data-loading-text="--><?php //echo $text_loading; ?><!--" class="btn btn-primary btn-lg btn-block"><i class="icon-cart hidden-md hidden-sm hidden-xs"></i>--><?php //echo $button_cart; ?><!--</button>-->

                            <button class='btn btn-default short-btn' onclick="get_popup_purchase('<?php echo $product_id; ?>');"><span><?php echo $button_short_by; ?></span></button>
                            <!-- AddThis Button BEGIN -->
                            <div class="share_wrapp">
                                <div class="text"><i class="icon-share"></i><?php echo $text_sharing; ?></div>
                                <div class="icon_wrapp">

                                    <div class="share_item facebook  icon-facebook">
                                        <a href="" onClick="openWin2();"></a>
                                    </div>
                                    <div class="share_item twitter icon-twitter">
                                        <a class=""
                                           href="https://twitter.com/share"
                                           data-size="large"
                                           data-text="custom share text"
                                           data-url="https://dev.twitter.com/web/tweet-button"
                                           data-hashtags="example,demo"
                                           data-via="twitterdev"
                                           data-related="twitterapi,twitter"
                                           target="_blank">
                                        </a>
                                    </div>
                                    <div class="share_item google icon-google-plus">
                                        <a href="https://plus.google.com/share?url=<?php echo $og_url; ?>" target="_blank"></a>
                                    </div>
                                </div>

                                <div class="hidden">
                                    <div class="g-plus" data-action="share"></div>
                                    <a href="https://twitter.com/share" class="twitter-share-button" data-lang="ru">Твитнуть</a>
                                </div>

                                <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];
                                        if(!d.getElementById(id))
                                        {js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";
                                            fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
                                </script>
                                <script>
                                    $('.facebook a').click(function (e) {
                                        e.preventDefault();
                                    })
                                    function openWin2() {
                                        myWin=open("http://www.facebook.com/sharer.php?u=<?php echo $og_url; ?>","displayWindow","width=520,height=300,left=350,top=170,status=no,toolbar=no,menubar=no");
                                    }
                                </script>
                            </div>
                            <!-- AddThis Button END -->
                        </div>


                        <?php if ($minimum > 1) { ?>
                            <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $text_minimum; ?></div>
                        <?php } ?>
                    </div>

                </div>
                <div class="col-md-12 col-sm-12  col-xs-12">
                    <div class="description_wrapper">
                        <ul class="nav nav-tabs article-tabs">
                            <li class="active"><a href="#tab-description" data-toggle="tab" data-hover="<?php echo $tab_description; ?>"><?php echo $tab_description; ?></a></li>
                            <?php if ($review_status) { ?>
                                <li><a href="#tab-review" data-toggle="tab" data-hover="<?php echo $tab_review; ?>"><?php echo $tab_review; ?></a></li>
                            <?php } ?>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab-description" itemprop="description"><?php echo $description; ?></div>
                            <?php if ($review_status) { ?>
                                <div class="tab-pane" id="tab-review">

                                    <form class="form-horizontal" id="form-review">
                                        <div id="review"></div>
                                        <div class="wrapper-review">
                                            <div class="text_write"><?php echo $text_write; ?></div>
                                            <?php if ($review_guest) { ?>
                                                <div class="form-group required">
                                                    <div class="col-sm-12">
                                                        <div class="reviewStars-input">
                                                            <input type="radio" name="rating" id="star-5" value="5"/>
                                                            <label for="star-5"
                                                                   class="star"><span class="hidden-xs"><?php echo $text_star_5; ?></span></label>
                                                            <input type="radio" name="rating" id="star-4" value="4"/>
                                                            <label for="star-4"
                                                                   class="star"><span class="hidden-xs"><?php echo $text_star_4; ?></span></label>
                                                            <input type="radio" name="rating" id="star-3" value="3"/>
                                                            <label for="star-3"
                                                                   class="star"><span class="hidden-xs"><?php echo $text_star_3; ?></span></label>
                                                            <input type="radio" name="rating" id="star-2" value="2"/>
                                                            <label for="star-2"
                                                                   class="star"><span class="hidden-xs"><?php echo $text_star_2; ?></span></label>
                                                            <input type="radio" name="rating" id="star-1" value="1"/>
                                                            <label for="star-1"
                                                                   class="star"><span class="hidden-xs"><?php echo $text_star_1; ?></span></label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group required">
                                                    <div class="col-sm-6 col-sm-offset-3">
                                                        <input type="text" name="name" value="" id="input-name_review"
                                                               placeholder="<?php echo $entry_name; ?>"
                                                               class="form-control"/>
                                                    </div>
                                                </div>
                                                <div class="form-group required">
                                                    <div class="col-sm-6 col-sm-offset-3">
                                                        <input type="text" name="email" value="" id="input-email_review"
                                                               placeholder="<?php echo $entry_email; ?>"
                                                               class="form-control"/>
                                                    </div>
                                                </div>
                                                <div class="form-group required">
                                                    <div class="col-sm-6 col-sm-offset-3">
                                                    <textarea name="text" rows="5" id="input-review"
                                                              placeholder="<?php echo $entry_review; ?>"
                                                              class="form-control"></textarea>
                                                    </div>
                                                </div>
                                                <?php echo $captcha; ?>
                                                <div class="buttons clearfix">
                                                    <div class="text-center">
                                                        <button type="button" id="button-review"
                                                                data-loading-text="<?php echo $text_loading; ?>"
                                                                class="btn btn-default"><span><?php echo $button_continue; ?></span></button>
                                                    </div>
                                                </div>
                                            <?php } else { ?>
                                                <div class="text_login_review">
                                                    <?php echo $text_login; ?>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </form>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>

            </div>
            <?php if (0) { ?>
                <h3><?php echo $text_related; ?></h3>
                <div class="row">
                    <?php $i = 0; ?>
                    <?php foreach ($products as $product) { ?>
                        <?php if ($column_left && $column_right) { ?>
                            <?php $class = 'col-lg-6 col-md-6 col-sm-12 col-xs-12'; ?>
                        <?php } elseif ($column_left || $column_right) { ?>
                            <?php $class = 'col-lg-4 col-md-4 col-sm-6 col-xs-12'; ?>
                        <?php } else { ?>
                            <?php $class = 'col-lg-3 col-md-3 col-sm-6 col-xs-12'; ?>
                        <?php } ?>
                        <div class="<?php echo $class; ?>">
                            <div class="product-layout col-md-4">
                                <div class="product-thumb transition ">

                                    <div class="image">
                                        <div class="button-group">
                                            <a href="<?php echo $product['href']; ?>"></a>
                                            <span class="plus"></span>
                                            <span class="plus"></span>
                                            <div class="wrapper-btn">
                                                <?php if ($product['wishlist_status']){ ?>
                                                    <button class="wish-btn active"
                                                            type="button" >
                                                        <i class="fa fa-heart" ></i>
                                                    </button>
                                                <?php } else { ?>
                                                    <button type="button" class="wish-btn"  onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="icon-heart-outline"></i></button>
                                                <?php } ?>
                                                <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');"><i class="icon-cart"></i> <?php echo $button_cart; ?></button>
                                            </div>
                                        </div>
                                        <a href="<?php echo $product['href']; ?>">

                                            <img src="<?php echo $product['thumb']; ?>"
                                                 alt="<?php echo $product['name']; ?>"
                                                 title="<?php echo $product['name']; ?>" class="img-responsive" />
                                        </a>

                                    </div>

                                    <div class="caption">
                                        <h4>
                                            <a href="<?php echo $product['href']; ?>">
                                                <?php echo $product['name']; ?>
                                            </a>
                                        </h4>
                                        <?php if ($product['model']) { ?>
                                            <span><?php echo $button_sku; ?><?php echo $product['model']; ?></span>
                                        <?php } ?>
                                        <?php if ($product['price']) { ?>
                                            <p class="price">
                                                <?php if (!$product['special']) { ?>
                                                    <?php echo $product['price']; ?>
                                                <?php } else { ?>
                                                    <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
                                                <?php } ?>
                                            </p>
                                        <?php } ?>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <?php if (($column_left && $column_right) && ($i % 2 == 0)) { ?>
                            <div class="clearfix visible-md visible-sm"></div>
                        <?php } elseif (($column_left || $column_right) && ($i % 3 == 0)) { ?>
                            <div class="clearfix visible-md"></div>
                        <?php } elseif ($i % 4 == 0) { ?>
                            <div class="clearfix visible-md"></div>
                        <?php } ?>
                        <?php $i++; ?>
                    <?php } ?>
                </div>
            <?php } ?>
            <?php if ($tags) { ?>
                <p><?php echo $text_tags; ?>
                    <?php for ($i = 0; $i < count($tags); $i++) { ?>
                        <?php if ($i < (count($tags) - 1)) { ?>
                            <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>,
                        <?php } else { ?>
                            <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>
                        <?php } ?>
                    <?php } ?>
                </p>
            <?php } ?>

        </div>
        <!--    --><?php //echo $column_right; ?>
    </div>
</div>
<?php echo $content_bottom; ?>
<script ><!--
    $('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
        $.ajax({
            url: 'index.php?route=product/product/getRecurringDescription',
            type: 'post',
            data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
            dataType: 'json',
            beforeSend: function() {
                $('#recurring-description').html('');
            },
            success: function(json) {
                $('.alert, .text-danger').remove();

                if (json['success']) {
                    $('#recurring-description').html(json['success']);
                }
            }
        });
    });

    $(function() {
        <?php if ($popup_purchase_data['allow_page']) { ?>
        $.each($("[onclick^='cart.add']"), function() {
            var product_id = $(this).attr('onclick').match(/[0-9]+/);
            $(this).parent().before("<div class='button-group popup-purchase-button'><button><?php echo $popup_purchase_text['button_purchase_now']; ?></button></div>").prev().attr('onclick', 'get_popup_purchase(' + product_id + ');');
        });
        $.each($(".product-thumb [onclick^='get_popup_cart']"), function() {
            var product_id = $(this).attr('onclick').match(/[0-9]+/);
            $(this).parent().before("<div class='button-group popup-purchase-button'><button><?php echo $popup_purchase_text['button_purchase_now']; ?></button></div>").prev().attr('onclick', 'get_popup_purchase(' + product_id + ');');
        });
        <?php } ?>
    });

    //--></script>
<script ><!--
    $('#button-cart').on('click', function() {
        $.ajax({
            url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
            dataType: 'json',
            beforeSend: function() {
                $('#button-cart').button('loading');
            },
            complete: function() {
                $('#button-cart').button('reset');
            },
            success: function(json) {
                $('.alert, .text-danger').remove();
                $('.form-group').removeClass('has-error');

                if (json['error']) {
                    if (json['error']['option']) {
                        for (i in json['error']['option']) {
                            var element = $('#input-option' + i.replace('_', '-'));

                            if (element.parent().hasClass('input-group')) {
                                element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                            } else {
                                element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                            }
                        }
                    }

                    if (json['error']['recurring']) {
                        $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
                    }

                    // Highlight any found errors
                    $('.text-danger').parent().addClass('has-error');
                }

                if (json['success']) {
                    $('header').before('<div class="alert alert-success"><div class="container">' + json['success'] + '</div><button type="button" class="close" data-dismiss="alert"><i class="icon-unchecked"></i></button></div>');
                    $('#cart').addClass('active');
                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                    setTimeout(function() {
                        $(".alert").alert('close');
                    }, 5000);
                    $('html, body').animate({ scrollTop: 0 }, 'slow');

                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
    //--></script>
<script ><!--
    $('button[id^=\'button-upload\']').on('click', function() {
        var node = this;

        $('#form-upload').remove();

        $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

        $('#form-upload input[name=\'file\']').trigger('click');

        if (typeof timer != 'undefined') {
            clearInterval(timer);
        }

        timer = setInterval(function() {
            if ($('#form-upload input[name=\'file\']').val() != '') {
                clearInterval(timer);

                $.ajax({
                    url: 'index.php?route=tool/upload',
                    type: 'post',
                    dataType: 'json',
                    data: new FormData($('#form-upload')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function() {
                        $(node).button('loading');
                    },
                    complete: function() {
                        $(node).button('reset');
                    },
                    success: function(json) {
                        $('.text-danger').remove();

                        if (json['error']) {
                            $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
                        }

                        if (json['success']) {
                            alert(json['success']);

                            $(node).parent().find('input').attr('value', json['code']);
                        }
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        }, 500);
    });
    //--></script>
<script ><!--
    $('#review').delegate('.pagination a', 'click', function(e) {
        e.preventDefault();

        $('#review').fadeOut('slow');

        $('#review').load(this.href);

        $('#review').fadeIn('slow');
    });

    $('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

    $('#button-review').on('click', function () {
        $.ajax({
            url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
            type: 'post',
            dataType: 'json',
            data: $("#form-review").serialize(),
            beforeSend: function () {
                $('#button-review').button('loading');
            },
            complete: function () {
                $('#button-review').button('reset');
            },
            success: function (json) {
                $('.alert-success, .alert-danger').remove();

                if (json['error']) {
                    $('#input-review').after('<div class="alert alert-danger"><div class="container">' + json['error'] + '</div><button type="button" class="close" data-dismiss="alert"><i class="icon-unchecked"></i></button></div>');
                }

                if (json['success']) {
                    $('#input-review').after('<div class="alert alert-success"><div class="container">' + json['success'] + '</div><button type="button" class="close" data-dismiss="alert"><i class="icon-unchecked"></i></button></div>');

                    $('input[name=\'name\']').val('');
                    $('textarea[name=\'text\']').val('');
                    $('input[name=\'email\']').val('');
                    $('input[name=\'rating\']:checked').prop('checked', false);
                }
            }
        });
    });

    // $(document).ready(function() {
    //     $('.thumbnails').magnificPopup({
    //         type:'image',
    //         delegate: 'a',
    //         gallery: {
    //             enabled:true
    //         }
    //     });
    // });
    //--></script>

<?php echo $footer; ?>
