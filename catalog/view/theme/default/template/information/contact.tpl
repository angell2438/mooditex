<?php echo $header; ?>
<div class="container">
          <ul  class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
            <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
              <?php if($i+1<count($breadcrumbs)) { ?>
              <a itemscope itemtype="http://schema.org/Thing" itemprop="item" href="<?php echo $breadcrumb['href']; ?>">
                <span itemprop="name"><?php echo $breadcrumb['text']; ?></span></a>
              <?php } else { ?><span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
              <?php } ?>
            </li>
            <?php } ?>
          </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="contact-wrapper <?php echo $class; ?>"><?php echo $content_top; ?>
      <h1><?php echo $heading_title; ?></h1>
          <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="info-shop">
                    <address>
                        <i class="icon-home"></i>
                        <span><?php echo $address; ?></span>
                    </address>
                    <div class="telephone-wrapp">
                        <i class="icon-phone"></i>
                        <a href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone3) ;?>">
                            <?php echo  $telephone3; ?>
                        </a>
                        <a href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone4) ;?>">
                            <?php echo  $telephone4; ?>
                        </a>
                    </div>

                </div>

                <div id="map1" class="map"></div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="info-shop">
                    <address>
                        <i class="icon-home"></i>
                        <span><?php echo $address2; ?></span>
                    </address>
                    <div class="telephone-wrapp">
                        <i class="icon-phone"></i>
                        <a href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone) ;?>">
                            <?php echo  $telephone; ?>
                        </a>
                        <a href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone2) ;?>">
                            <?php echo  $telephone2; ?>
                        </a>
                    </div>

                </div>
                <div id="map2" class="map"></div>
            </div>
              <div class="col-md-12">
                  <div class="contact-mail">
                      <p><?php echo $text_store; ?></p>
                      <a href="mailto:<?php echo $mail; ?>"><i class="icon-close-envelope"></i><?php echo $mail; ?></a>
                      <a href="mailto:<?php echo $fax; ?>"><i class="icon-close-envelope"></i><?php echo $fax; ?></a>
                  </div>
              </div>
          </div>
        <script
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyALGbIxt_aCHqxFzSPX-KopFPL5BvkRsHg&callback=initMap">
        </script>
        <script>

            function initialize() {
                var myLatlng = new google.maps.LatLng(<?php echo $geocode; ?>);
                var myOptions = {
                    zoom: 17,
                    center: myLatlng,
                    scrollwheel: false,
                    zoomControl: false,
                    mapTypeControl: false,
                    scaleControl: false,
                    streetViewControl: false,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    styles: [{"stylers":[{"saturation":-100},{"gamma":0.8},{"lightness":4},{"visibility":"on"}]},{"featureType":"landscape.natural","stylers":[{"visibility":"on"},{"color":"#5dff00"},{"gamma":4.97},{"lightness":-5},{"saturation":100}]}]

                }
                var map = new google.maps.Map(document.getElementById("map1"), myOptions);
                var image = '/image/natali_map_marker.png';
                var myLatlng = new google.maps.LatLng(<?php echo $geocode; ?>);
                var marker = new google.maps.Marker({
                    position: myLatlng,
                    map: map,
                    title:"",
                    icon: image
                });
            }

            google.maps.event.addDomListener(window, 'load', initialize);

            function initialize2() {
                var myLatlng = new google.maps.LatLng(<?php echo $geocode2; ?>);
                var myOptions = {
                    zoom: 17,
                    center: myLatlng,
                    scrollwheel: false,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    fullscreenControl: false,
                    zoomControl: false,
                    mapTypeControl: false,
                    scaleControl: false,
                    streetViewControl: false,
                    styles: [{"stylers":[{"saturation":-100},{"gamma":0.8},{"lightness":4},{"visibility":"on"}]},{"featureType":"landscape.natural","stylers":[{"visibility":"on"},{"color":"#5dff00"},{"gamma":4.97},{"lightness":-5},{"saturation":100}]}]
                }
                var map = new google.maps.Map(document.getElementById("map2"), myOptions);
                var image = '/image/natali_map_marker.png';
                var myLatlng = new google.maps.LatLng(<?php echo $geocode2; ?>);
                var marker = new google.maps.Marker({
                    position: myLatlng,
                    map: map,
                    title:"",
                    icon: image
                });
            }
            google.maps.event.addDomListener(window, 'load', initialize2);
        </script>

      <?php echo $content_bottom; ?>
    </div>
    <?php echo $column_right; ?>
  </div>
</div>

<?php echo $footer; ?>
