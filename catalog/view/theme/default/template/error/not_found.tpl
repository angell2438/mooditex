<?php echo $header; ?>
<div class="error-wrapper">
    <img class="error-img" src="catalog/view/image/img-2386.png" alt="404">
    <div class="text-error">
        <img src="image/catalog/bitmap-copy.png" alt="logo">
        <p>404 <span><?php echo $heading_title; ?></span></p>
        <div class="buttons">
            <a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $text_error; ?></a>
        </div>

    </div>


</div>
      
<?php echo $footer; ?>