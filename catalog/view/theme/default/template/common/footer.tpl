</div>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-12">
                <div class="footer-info">
                    <img src="image/catalog/bitmap_12.png" alt="wooditex">
                    <div class="mail-container">
                        <a href="mailto:<?php echo $mail; ?>"><i class="icon-close-envelope"></i><?php echo $mail; ?></a>
                        <a href="mailto:<?php echo $mail; ?>"><i class="icon-close-envelope"></i><?php echo $mail; ?></a>
                    </div>
                    <ul class="clearfix">
                        <li><a href="<?php echo $config_fb; ?>" target="_blank"><i class="icon-facebook"></i></a></li>
                        <li><a href="<?php echo $config_inst; ?>" target="_blank"><i class="icon-instagram"></i></a></li>
                        <li><a href="<?php echo $config_you; ?>" target="_blank"><i class="icon-youtube"></i></a></li>
                        <li><a href="<?php echo $config_gp; ?>" target="_blank"><i class="icon-google-plus"></i></a></li>
                        <li><a href="<?php echo $config_twitter; ?>" target="_blank"><i class="icon-twitter"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-1 col-md-0"></div>
            <?php if ($categories) { ?>
                <div class="col-lg-3 col-md-4 col-sm-4 parent-elements">
                    <h5 class="footer-title">
                        <?php echo $text_service; ?>
                        <i class="visible-xs fa fa-angle-down" aria-hidden="true"></i>
                    </h5>
                    <ul class="list-unstyled">
                        <?php foreach ($categories as $category) { ?>
                            <?php if ($category['children']) { ?>
                                <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
                                    <?php foreach ($children as $child) { ?>
                                        <li>
                                            <a href="<?php echo $child['href']; ?>" data-hover="<?php echo $child['name']; ?>">
                                                <?php echo $child['name']; ?>
                                            </a>
                                        </li>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                    </ul>
                </div>
            <?php } ?>
            <?php if ($informations) { ?>
                <div class="col-lg-2 col-md-4 col-sm-3 parent-elements col-xs-12">
                    <h5 class="footer-title">
                        <?php echo $text_information; ?>
                        <i class="visible-xs fa fa-angle-down" aria-hidden="true"></i>
                    </h5>
                    <ul class="list-unstyled">
                        <?php foreach ($informations as $information) { ?>
                            <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
                        <?php } ?>
                        <?php foreach ($ncategories as $ncategory) { ?>
                            <li><a href="<?php echo $ncategory['href']; ?>"><?php echo $ncategory['name']; ?></a></li>
                        <?php } ?>
                        <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
                    </ul>
                </div>
            <?php } ?>
            <div class="col-lg-3 col-md-4 col-sm-5 col-xs-12">
                <div class="footer-info info-shops parent-elements">
                    <h5 class="footer-title">
                        <?php echo $text_contact; ?>
                        <i class="visible-xs fa fa-angle-down" aria-hidden="true"></i>
                    </h5>
                    <div class="list-unstyled">
                        <p><i class="icon-home"></i><?php echo $address; ?></p>
                            <div class="phone-wrapper">
                                <i class="icon-phone"></i>
                                <div class="phone-items">
                                    <a class="bold-text" href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone3 ) ;?>"><?php echo $telephone3; ?></a>
                                    <a class="bold-text" href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone4 ) ;?>"><?php echo $telephone4; ?></a>
                                </div>
                            </div>
                    </div>
                </div>
                <hr>
                <div class="footer-info info-shops parent-elements">
                    <div class="list-unstyled">
                        <p><i class="icon-home"></i><?php echo $address2; ?></p>

                        <div class="phone-wrapper">
                            <i class="icon-phone"></i>
                            <div class="phone-items">
                                <a class="bold-text" href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone ) ;?>"><?php echo $telephone; ?></a>
                                <a class="bold-text" href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone2 ) ;?>"><?php echo $telephone2; ?></a>

                            </div>
                          </div>
                    </div>

                </div>
            </div>

        </div>
        <div class="row">
            <hr>
            <div class="double-footer clearfix">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="row">
                        <p><?php echo $powered; ?></p>
                    </div>
                </div>
                <div class="col-md-6  col-sm-6 col-xs-12 text-right">
                    <div class="row">
                        <a href="http://web-systems.solutions/" target="_blank">
                            <img src="/image/catalog/copyright.svg" alt="web-systems.solutions" title="web-systems.solutions" width="151" height="19">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

</div>

<?//php echo $quicksignup; ?>
<script src="catalog/view/javascript/jquery/slick/slick.js"></script>
<script src="catalog/view/javascript/jquery/jquery.maskedinput.min.js"></script>
<script src="catalog/view/javascript/jquery/select2.min.js"></script>
<script src="catalog/view/javascript/jquery/machheigh.js"></script>
<script src="catalog/view/javascript/dotdotdot.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/latest/TweenMax.min.js"></script>
<script src="catalog/view/javascript/jquery/jquery.elevateZoom-3.0.8.min.js"></script>
<script src="catalog/view/javascript/jquery-modal-video.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lightgallery.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-fullscreen.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-thumbnail.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-video.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-autoplay.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-zoom.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-hash.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-pager.js"></script>
<script src="catalog/view/javascript/main.js"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyALGbIxt_aCHqxFzSPX-KopFPL5BvkRsHg&extension=.js"></script>
<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>

</body>
</html>