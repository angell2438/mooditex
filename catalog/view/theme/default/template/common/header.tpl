<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?></title>
    <base href="<?php echo $base; ?>" />
    <?php if ($description) { ?>
        <meta name="description" content="<?php echo $description; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
    <?php } ?>
    <?php if ($keywords) { ?>
        <meta name="keywords" content= "<?php echo $keywords; ?>" />
    <?php } ?>
    <meta property="og:title" content="<?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="<?php echo $og_url; ?>" />
    <?php if ($og_image) { ?>
        <meta property="og:image" content="<?php echo $og_image; ?>" />
    <?php } else { ?>
        <meta property="og:image" content="<?php echo $logo; ?>" />
    <?php } ?>
    <meta property="og:site_name" content="<?php echo $name; ?>" />
    <script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js"></script>
    <link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
    <script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js"></script>
    <link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <?php foreach ($styles as $style) { ?>
        <link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
    <?php } ?>
    <link href="catalog/view/javascript/jquery/slick/slick-theme.css" rel="stylesheet">
    <link href="catalog/view/javascript/jquery/slick/slick.css" rel="stylesheet">
    <link href="catalog/view/theme/default/stylesheet/select2.min.css" rel="stylesheet">
    <link href="catalog/view/theme/default/stylesheet/lightgallery.css" rel="stylesheet">
    <link href="catalog/view/theme/default/stylesheet/modal-video.min.css" rel="stylesheet">
    <link href="catalog/view/theme/default/stylesheet/main.css" rel="stylesheet">
    <link href="catalog/view/theme/default/stylesheet/media.css" rel="stylesheet">
    <link href="catalog/view/theme/default/stylesheet/popup_purchase/stylesheet.css" rel="stylesheet" media="screen" />
    <script src="catalog/view/javascript/common.js" ></script>

    <?php foreach ($links as $link) { ?>
        <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
    <?php } ?>
    <?php foreach ($scripts as $script) { ?>
        <script src="<?php echo $script; ?>"></script>
    <?php } ?>
    <?php foreach ($analytics as $analytic) { ?>
        <?php echo $analytic; ?>
    <?php } ?>
</head>
<body class="<?php echo $class; ?>">
<div class="menu-black">
    <div class="container">
        <div class="row">
            <div class="col-md-2 col-md-offset-5 col-xs-6 col-xs-offset-3">
                <div class="row">
                    <img src="image/catalog/bitmap-copy.png" alt="logo">
                </div>
            </div>
            <div class="col-md-12">
                <ul class="clearfix active-menu">
                        <?php if ($informations) { ?>
                            <?php foreach ($informations as $information) { ?>
                                <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
                            <?php } ?>
                            <?php foreach ($ncategories as $ncategory) { ?>
                                <li><a href="<?php echo $ncategory['href']; ?>"><?php echo $ncategory['name']; ?></a></li>
                            <?php } ?>
                            <li><a href="<?php echo $contact; ?>"><?//php echo $text_contact; ?></a></li>
                        <?php } ?>
                </ul>
                <div class="wrapper-language">
                        <?php echo $language; ?>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="page-wrapper panel-overlay">
    <div class="phone-wrapper_bg visible-md visible-sm visible-xs">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                        <div class="phone">
                            <span><?php echo $text_city1; ?></span>
                            <a href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone3) ;?>">
                                <?php echo  $telephone3; ?>
                            </a>
                            <a href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone4) ;?>">
                                <?php echo  $telephone4; ?>
                            </a>
                        </div>
                </div>
                <div class="col-md-6">
                        <div class="phone text-right">
                            <span><?php echo $text_city2; ?></span>
                            <a href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone) ;?>">
                                <?php echo  $telephone; ?>
                            </a>
                            <a href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone2) ;?>">
                                <?php echo  $telephone2; ?>
                            </a>
                        </div>
                </div>
            </div>
        </div>

    </div>
    <header>
        <div class="container">
            <div class="row flex-container">
                <div class="col-lg-5 col-md-2 col-sm-2 col-xs-2">
                        <div class="flex-wrapper">
                            <div class="menu ninja-btn">
                                <i class="icon-menu"></i>
                                <i class="icon-close"></i>
                            </div>
                            <div class="phone hidden-md hidden-sm hidden-xs">
                                <span><?php echo $text_city1; ?></span>
                                <a href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone3) ;?>">
                                    <?php echo  $telephone3; ?>
                                </a>
                                <a href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone4) ;?>">
                                    <?php echo  $telephone4; ?>
                                </a>
                            </div>
                        </div>
                </div>
                <div class="col-lg-2 col-md-8 col-sm-8 col-xs-8 js-position_menu">
                    <div class="row">
                        <div id="logo">
                            <?php if ($logo) { ?>
                                <?php if ($home == $og_url) { ?>
                                    <!--                                <img src="image/catalog/logo2mini.png"-->
                                    <!--                                     title="--><?php //echo $name; ?><!--"-->
                                    <!--                                     alt="--><?php //echo $name; ?><!--" class="img-responsive visible-xs" />-->


                                    <section class="letter">
                                        <svg id="Слой_1" class="letter-svg letter-svg-3 " data-name="Слой 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1005.45 404.69">
                                            <g id="Page-1" fill="none" fill-rule="evenodd">
                                                <title>logo</title>
                                                <path id="building1" class="cls-1 outer_line"
                                                      d="M115.28,353.55l36.37,92.16,37.19-98.77,36,98.77,37.61-92.16h22.73l-61.58,146.7-36-97.73-36.37,97.73L92.55,353.55Z"
                                                      transform="translate(-92.55 -218.56)"/>
                                                <path id="building2" class="cls-1 outer_line"
                                                      d="M435.14,422.56c0,40.5-30.37,71.49-71.7,71.49s-71.7-31-71.7-71.49,30.37-71.49,71.7-71.49S435.14,382.06,435.14,422.56Zm-21.08,0c0-30.58-21.08-52.07-50.62-52.07S312.82,392,312.82,422.56s21.08,52.07,50.62,52.07S414.06,453.14,414.06,422.56Z"
                                                      transform="translate(-92.55 -218.56)"/>
                                                <path id="building3" class="cls-1 outer_line"
                                                      d="M598.37,422.56c0,40.5-30.37,71.49-71.7,71.49s-71.7-31-71.7-71.49,30.38-71.49,71.7-71.49S598.37,382.06,598.37,422.56Zm-21.08,0c0-30.58-21.08-52.07-50.62-52.07S476.05,392,476.05,422.56s21.08,52.07,50.62,52.07S577.3,453.14,577.3,422.56Z"
                                                      transform="translate(-92.55 -218.56)"/>
                                                <path id="building4" class="cls-1 outer_line"
                                                      d="M653.75,353.55c19.22,0,36,2.07,51.24,13.43,16.74,12.6,26.45,32.65,26.45,55.58s-9.5,42.77-27.69,55.58c-16.12,11.36-31.2,13.22-50.42,13.22H624.83V353.55Zm-7.85,118H655c10.33,0,24.59-.82,36.57-9.71,9.5-7.23,18.8-20.46,18.8-39.26,0-18.18-8.68-32.44-18.6-39.67-12-8.68-26.65-9.51-36.78-9.51H645.9Z"
                                                      transform="translate(-92.55 -218.56)"/>
                                                <path id="building5" class="cls-1 outer_line"
                                                      d="M976.5,373.38h-55V408.3h53.31v19.84H921.54v43.39h55v19.84h-76V353.55h76Z"
                                                      transform="translate(-92.55 -218.56)"/>
                                                <path id="building6" class="cls-1 outer_line"
                                                      d="M1032.71,419.46l-38-65.91h23.56l26.86,46.7,27.48-46.7h23.56l-39.67,65.91L1098,491.37h-23.56l-30.17-52.69-31.41,52.69H989.31Z"
                                                      transform="translate(-92.55 -218.56)"/>
                                                <path id="building7" class="cls-2 outer_line"
                                                      d="M880,550.88c-2.82-1.14-3.94-2.84-4.29-4.45A46,46,0,0,1,881,535.77a17.31,17.31,0,0,1,4.61-4.62,30.46,30.46,0,0,1,5.05-2.92,22.22,22.22,0,0,1,6.29-1.74c.78-.1,2.18-.3,3.87-.49a26.78,26.78,0,0,0,5.57-1,56.28,56.28,0,0,0,7.05-2.51,55.87,55.87,0,0,1-7,1.45,27,27,0,0,1-5.39.27,33.2,33.2,0,0,0-3.91.16,25.71,25.71,0,0,0-17.6,9.55,43.08,43.08,0,0,0-4.27,7.13,5.76,5.76,0,0,1-1.13-4.72c.65-4,4.51-15.32,15.28-18.94s20.19.72,25.46.32,5.46,1.53,5.46,1.53S905.94,555.53,880,550.88Z"
                                                      transform="translate(-92.55 -218.56)"/>
                                                <polygon id="building8" class="cls-1 outer_line"
                                                         points="789.32 154.83 789.32 134.99 705.01 134.99 705.01 154.83 736.63 154.83 736.63 404.69 757.71 404.69 757.71 370.02 784.75 342.98 769.67 327.89 757.71 339.86 757.71 154.83 789.32 154.83"/>
                                                <polygon id="building9" class="cls-1 outer_line"
                                                         points="665.34 59.92 665.34 95.75 652.03 82.44 636.94 97.52 665.34 125.93 665.34 272.81 686.42 272.81 686.42 59.92 665.34 59.92"/>
                                                <path id="building10" class="cls-2 outer_line"
                                                      d="M765.79,268.15c3.8.6,6-.2,7.36-1.37a37,37,0,0,0,.28-12,16.44,16.44,0,0,0-2.46-6.48,34.74,34.74,0,0,0-3.93-5.29,30.13,30.13,0,0,0-6-5c-.81-.52-2.26-1.47-4-2.57a37.86,37.86,0,0,1-5.61-4,72.14,72.14,0,0,1-6.4-6.06,77.71,77.71,0,0,0,7,5.14,40.42,40.42,0,0,0,5.84,3.23c1.87.92,3.42,1.78,4.27,2.32,1.56,1.06,11.15,6.45,14.07,17.89a35.62,35.62,0,0,1,.63,8.41,5.78,5.78,0,0,0,4-3.36,22.41,22.41,0,0,0-6-24.53c-9.9-9.06-22.88-10.65-28.52-13.93s-7-1.75-7-1.75S734.2,257.61,765.79,268.15Z"
                                                      transform="translate(-92.55 -218.56)"/>
                                            </g>
                                        </svg>
                                    </section>

<!--                                    <img src="--><?php //echo $logo; ?><!--" title="--><?php //echo $name; ?><!--" alt="--><?php //echo $name; ?><!--" class="img-responsive hidden-xs" />-->
                                <?php } else { ?>
                                    <a href="<?php echo $home; ?>">
                                        <!--                                    <img src="image/catalog/logo2mini.png"-->
                                        <!--                                         title="--><?php //echo $name; ?><!--"-->
                                        <!--                                         alt="--><?php //echo $name; ?><!--" class="img-responsive visible-xs" />-->

                                        <section class="letter">
                                            <svg id="Слой_1" class="letter-svg letter-svg-3 " data-name="Слой 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1005.45 404.69">

                                                <g id="Page-1" fill="none" fill-rule="evenodd">
                                                    <title>logo</title>
                                                    <path id="building1" class="cls-1 outer_line"
                                                          d="M115.28,353.55l36.37,92.16,37.19-98.77,36,98.77,37.61-92.16h22.73l-61.58,146.7-36-97.73-36.37,97.73L92.55,353.55Z"
                                                          transform="translate(-92.55 -218.56)"/>
                                                    <path id="building2" class="cls-1 outer_line"
                                                          d="M435.14,422.56c0,40.5-30.37,71.49-71.7,71.49s-71.7-31-71.7-71.49,30.37-71.49,71.7-71.49S435.14,382.06,435.14,422.56Zm-21.08,0c0-30.58-21.08-52.07-50.62-52.07S312.82,392,312.82,422.56s21.08,52.07,50.62,52.07S414.06,453.14,414.06,422.56Z"
                                                          transform="translate(-92.55 -218.56)"/>
                                                    <path id="building3" class="cls-1 outer_line"
                                                          d="M598.37,422.56c0,40.5-30.37,71.49-71.7,71.49s-71.7-31-71.7-71.49,30.38-71.49,71.7-71.49S598.37,382.06,598.37,422.56Zm-21.08,0c0-30.58-21.08-52.07-50.62-52.07S476.05,392,476.05,422.56s21.08,52.07,50.62,52.07S577.3,453.14,577.3,422.56Z"
                                                          transform="translate(-92.55 -218.56)"/>
                                                    <path id="building4" class="cls-1 outer_line"
                                                          d="M653.75,353.55c19.22,0,36,2.07,51.24,13.43,16.74,12.6,26.45,32.65,26.45,55.58s-9.5,42.77-27.69,55.58c-16.12,11.36-31.2,13.22-50.42,13.22H624.83V353.55Zm-7.85,118H655c10.33,0,24.59-.82,36.57-9.71,9.5-7.23,18.8-20.46,18.8-39.26,0-18.18-8.68-32.44-18.6-39.67-12-8.68-26.65-9.51-36.78-9.51H645.9Z"
                                                          transform="translate(-92.55 -218.56)"/>
                                                    <path id="building5" class="cls-1 outer_line"
                                                          d="M976.5,373.38h-55V408.3h53.31v19.84H921.54v43.39h55v19.84h-76V353.55h76Z"
                                                          transform="translate(-92.55 -218.56)"/>
                                                    <path id="building6" class="cls-1 outer_line"
                                                          d="M1032.71,419.46l-38-65.91h23.56l26.86,46.7,27.48-46.7h23.56l-39.67,65.91L1098,491.37h-23.56l-30.17-52.69-31.41,52.69H989.31Z"
                                                          transform="translate(-92.55 -218.56)"/>
                                                    <path id="building7" class="cls-2 outer_line"
                                                          d="M880,550.88c-2.82-1.14-3.94-2.84-4.29-4.45A46,46,0,0,1,881,535.77a17.31,17.31,0,0,1,4.61-4.62,30.46,30.46,0,0,1,5.05-2.92,22.22,22.22,0,0,1,6.29-1.74c.78-.1,2.18-.3,3.87-.49a26.78,26.78,0,0,0,5.57-1,56.28,56.28,0,0,0,7.05-2.51,55.87,55.87,0,0,1-7,1.45,27,27,0,0,1-5.39.27,33.2,33.2,0,0,0-3.91.16,25.71,25.71,0,0,0-17.6,9.55,43.08,43.08,0,0,0-4.27,7.13,5.76,5.76,0,0,1-1.13-4.72c.65-4,4.51-15.32,15.28-18.94s20.19.72,25.46.32,5.46,1.53,5.46,1.53S905.94,555.53,880,550.88Z"
                                                          transform="translate(-92.55 -218.56)"/>
                                                    <polygon id="building8" class="cls-1 outer_line"
                                                             points="789.32 154.83 789.32 134.99 705.01 134.99 705.01 154.83 736.63 154.83 736.63 404.69 757.71 404.69 757.71 370.02 784.75 342.98 769.67 327.89 757.71 339.86 757.71 154.83 789.32 154.83"/>
                                                    <polygon id="building9" class="cls-1 outer_line"
                                                             points="665.34 59.92 665.34 95.75 652.03 82.44 636.94 97.52 665.34 125.93 665.34 272.81 686.42 272.81 686.42 59.92 665.34 59.92"/>
                                                    <path id="building10" class="cls-2 outer_line"
                                                          d="M765.79,268.15c3.8.6,6-.2,7.36-1.37a37,37,0,0,0,.28-12,16.44,16.44,0,0,0-2.46-6.48,34.74,34.74,0,0,0-3.93-5.29,30.13,30.13,0,0,0-6-5c-.81-.52-2.26-1.47-4-2.57a37.86,37.86,0,0,1-5.61-4,72.14,72.14,0,0,1-6.4-6.06,77.71,77.71,0,0,0,7,5.14,40.42,40.42,0,0,0,5.84,3.23c1.87.92,3.42,1.78,4.27,2.32,1.56,1.06,11.15,6.45,14.07,17.89a35.62,35.62,0,0,1,.63,8.41,5.78,5.78,0,0,0,4-3.36,22.41,22.41,0,0,0-6-24.53c-9.9-9.06-22.88-10.65-28.52-13.93s-7-1.75-7-1.75S734.2,257.61,765.79,268.15Z"
                                                          transform="translate(-92.55 -218.56)"/>
                                                </g>
                                            </svg>
                                        </section>


                                        <!--                                        <img src="--><?php //echo $logo; ?><!--"-->
<!--                                             title="--><?php //echo $name; ?><!--" alt="--><?php //echo $name; ?><!--" class="img-responsive hidden-xs" />-->
                                    </a>
                                <?php } ?>
                            <?php } else { ?>
                                <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-md-2 col-sm-2 col-xs-2 js-container">
<!--                    <div class="row">-->
                        <div class="flex-wrapper flex-right">
                            <div class="phone hidden-md hidden-sm hidden-xs">
                                <span><?php echo $text_city2; ?></span>
                                <a href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone) ;?>">
                                    <?php echo  $telephone; ?>
                                </a>
                                <a href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone2) ;?>">
                                    <?php echo  $telephone2; ?>
                                </a>
                            </div>
                            <div class="items-help_elements search_container">
                                <?php echo $search; ?>
                            </div>
                        </div>
<!--                    </div>-->
                </div>
            </div>
        </div>
    </header>
    <div class="category-block">
        <div class="container">
            <div class="row">
                <?php if ($categories) { ?>
                    <div class="menu-container">
                        <nav id="menu" class="navbar">
                            <div class="navbar-header">
                                <button type="button" class="btn btn-navbar navbar-toggle"
                                        data-toggle="collapse" data-target=".navbar-ex1-collapse">
                                    <span id="category" class="visible-md visible-sm visible-xs"><?php echo $text_category; ?></span>
                                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                                </button>

                            </div>

                            <div class="collapse navbar-collapse navbar-ex1-collapse">
                                <ul class="nav navbar-nav active-menu">
                                    <?php foreach ($categories as $category) { ?>
                                        <?php if ($category['children']) { ?>
                                            <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
                                                <?php foreach ($children as $child) { ?>
                                                <li>
                                                    <a href="<?php echo $child['href']; ?>" data-hover="<?php echo $child['name']; ?>">
                                                        <?php echo $child['name']; ?>
                                                    </a>
                                                </li>
                                                <?php } ?>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </ul>
                            </div>
                        </nav>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="main-content">

