<?php if (count($languages) > 1) { ?>
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="language">
        <ul class="list-unstyled list-inline">
            <?php foreach ($languages as $language) { ?>
                <li class="<?php if ($language['code'] == $code) { ?> active<?php } ?>"><a href="<?php echo $language['code']; ?>"><?php echo $language['code']; ?></a></li>
            <?php } ?>
        </ul>
        <input type="hidden" name="code" value="" />
        <input type="hidden" name="redirect_route" value="<?php echo $redirect_route; ?>" />
        <input type="hidden" name="redirect_query" value="<?php echo isset($redirect_query) ? $redirect_query : ''; ?>" />
        <input type="hidden" name="redirect_ssl" value="<?php echo isset($redirect_ssl) ? $redirect_ssl : ''; ?>" />
    </form>
<?php } ?>
