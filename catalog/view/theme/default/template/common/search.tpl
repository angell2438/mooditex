<div id="search" class="input-group">


    <div class="header_search">
        <span class="icon icon-search"></span>
        <span class="button-ok">
            Ок
        </span>
    </div><!-- end header_search -->

    <div class="search_block_content">
        <input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_search; ?>" class="form-control input-lg" id="input-search" />
    </div><!-- end search_block_content -->

</div><!-- end search -->
