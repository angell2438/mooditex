<div class="catalog_page_wrapp">
    <?php if($catalogs_list) { ?>
        <div class="catalogs_list">
            <div class="row">
                <?php foreach($catalogs_list as $catalog) { ?>
                <div class="col-md-3 col-sm-4 col-xs-6 col-xxs-12 catalog_layout">
                    <div class="catalog_thumb<?php echo ($catalog['active'])? " active":""?>">
                        <img src="<?php echo $catalog['image']; ?>" class="img-responsive" alt="<?php echo $catalog['title']; ?>">
                        <div class="text"><?php echo $catalog['title']; ?></div>
                        <a href="<?php echo $catalog['link']; ?>">
                        </a>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    <?php } ?>
    <?php if($products_list){ ?>
        <div class="title2"><?php echo $heading_title; ?></div>
        <?php if($catalog_file){ ?>
        <div class="download_link"><a href="/system/storage/download/<?php echo $catalog_file; ?>"  target="_blank"><?php echo $text_download; ?></a></div>
        <?php } ?>
        <div class="products_list product-list">
            <?php foreach ($products_list as $product) { ?>
                <div class="product-layout">
                    <div class="product-thumb transition">
                        <div class="image">
                            <a href="<?php echo $product['href']; ?>">
                                <img src="<?php echo $product['thumb']; ?>"
                                     alt="<?php echo $product['name']; ?>"
                                     title="<?php echo $product['name']; ?>" class="img-responsive" />
                            </a>
                        </div>
                        <div class="caption">
                            <h4>
                                <a href="<?php echo $product['href']; ?>">
                                    <?php echo $product['name']; ?>
                                </a>
                            </h4>
                            <?php if ($product['model']) { ?>
                                <span><?php echo $button_sku; ?><?php echo $product['model']; ?></span>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    <?php } ?>
</div>
