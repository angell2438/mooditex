<?php if ($article) { ?>
    <div class="news-content gallery-category">
        <div class="row">
            <div class="gallery_items  position-date bnews-list<?php if ($display_style) { ?> bnews-list-2<?php } ?> news-list ">
                <?php foreach ($article as $articles) { ?>
                      <a  class="col-md-4" href="<?php echo $articles['popup']; ?>">
                         <?php if ($articles['thumb']) { ?>
                           <img class="article-image" src="<?php echo $articles['thumb']; ?>" title="<?php echo $articles['name']; ?>" alt="<?php echo $articles['name']; ?>" />
                        <?php } ?>
                      </a>
                <?php } ?>
            </div>
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 text-right"><?php echo $pagination; ?></div>
    </div>
	<script type="text/javascript"><!--
	$(document).ready(function() {
        $(document).ready(function() {
            $('.gallery_items').lightGallery({
                loop: true,
                fourceAutoply: true,
                thumbnail: true,
                hash: false,
                speed: 400,
                scale: 1,
                keypress: true,
                counter: false,
                download: false,
            });
        });
		$('img.article-image').each(function(index, element) {
		var articleWidth = $(this).parent().parent().width() * 0.7;
		var imageWidth = $(this).width() + 10;
		if (imageWidth >= articleWidth) {
			$(this).attr("align","center");
			$(this).parent().addClass('bigimagein');
		}
		});
		$(window).resize(function() {
		$('img.article-image').each(function(index, element) {
		var articleWidth = $(this).parent().parent().width() * 0.7;
		var imageWidth = $(this).width() + 10;
		if (imageWidth >= articleWidth) {
			$(this).attr("align","center");
			$(this).parent().addClass('bigimagein');
		}
		});
		});
	});
	//--></script> 
<?php } ?>
<?php if ($is_category) { ?>
  <?php if (!$ncategories && !$article) { ?>
  <div class="content"><?php echo $text_empty; ?></div>
  <?php } ?>
<?php } else { ?>
  <?php if (!$article) { ?>
  <div class="content"><?php echo $text_empty; ?></div>
  <?php } ?>
<?php } ?>
<?php if ($disqus_status) { ?>
<script type="text/javascript">
var disqus_shortname = '<?php echo $disqus_sname; ?>';
(function () {
var s = document.createElement('script'); s.async = true;
s.type = 'text/javascript';
s.src = 'http://' + disqus_shortname + '.disqus.com/count.js';
(document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
}());
</script>
<?php } ?>
<?php if ($fbcom_status) { ?>
<script type="text/javascript">
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '<?php echo $fbcom_appid; ?>',
		  status     : true,
          xfbml      : true,
		  version    : 'v2.0'
        });
      };

      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
</script>
<?php } ?>