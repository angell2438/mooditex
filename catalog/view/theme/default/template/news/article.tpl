<?php if($ncat_info['ncategory_id'] == 63) { ?>
    <div class="container">
            <div class="article-content">
                <div class="wrapper-image_article">
                    <?php if ($thumb) { ?>
                        <a href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>" class="colorbox">
                            <img align="left" src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>"
                                 alt="<?php echo $heading_title; ?>" id="image-article" />
                            <h1><?php echo $heading_title; ?></h1>
                        </a>

                    <?php } ?>
                </div>
                <div class="article-description">
                    <?php echo $description; ?>
                </div>
                <!-- AddThis Button BEGIN -->
                <div class="share_wrapp">
                    <div class="text"><i class="icon-share"></i><?php echo $text_sharing; ?></div>
                    <div class="icon_wrapp">

                        <div class="share_item facebook  icon-facebook">
                            <a href="" onClick="openWin2();"></a>
                        </div>
                        <div class="share_item twitter icon-twitter">
                            <a class=""
                               href="https://twitter.com/share"
                               data-size="large"
                               data-text="custom share text"
                               data-url="https://dev.twitter.com/web/tweet-button"
                               data-hashtags="example,demo"
                               data-via="twitterdev"
                               data-related="twitterapi,twitter"
                               target="_blank">
                            </a>
                        </div>
                        <div class="share_item google icon-google-plus">
                            <a href="https://plus.google.com/share?url=<?php echo $og_url; ?>" target="_blank"></a>
                        </div>
                    </div>

                    <div class="hidden">
                        <div class="g-plus" data-action="share"></div>
                        <a href="https://twitter.com/share" class="twitter-share-button" data-lang="ru">Твитнуть</a>
                    </div>

                    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];
                            if(!d.getElementById(id))
                            {js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";
                                fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
                    </script>
                    <script>
                        $('.facebook a').click(function (e) {
                            e.preventDefault();
                        })
                        function openWin2() {
                            myWin=open("http://www.facebook.com/sharer.php?u=<?php echo $og_url; ?>","displayWindow","width=520,height=300,left=350,top=170,status=no,toolbar=no,menubar=no");
                        }
                    </script>
                </div>
                <!-- AddThis Button END -->
                <?php if ($article_videos) { ?>
                    <div class="content blog-videos">
                        <?php foreach ($article_videos as $video) { ?>
                            <?php echo ($video['text']) ? '<h2 class="video-heading">'.$video['text'].'</h2>' : ''; ?>
                            <div>
                                <?php echo $video['code']; ?>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
    </div>
    <?php if ($article_new) { ?>
        <div class="wrapper-news viewed-product">
            <!--    <div class="container">-->
            <h2><?php echo $news_related; ?></h2>
            <!--        <div class="row">-->
            <div  class="news-content">
                <div id='news-item' class="news-list  viewed-news slick-wrapper clearfix">
                    <?php foreach ($article_new as $articles) { ?>
                        <div class="news-block col-md-4">
                            <div class="position-date">
                                <a class="" href="<?php echo $articles['href']; ?>">
                                    <?php if ($articles['thumb']) { ?>
                                        <img class="article-image" src="<?php echo $articles['thumb']; ?>" title="<?php echo $articles['name']; ?>" alt="<?php echo $articles['name']; ?>" />
                                    <?php } ?>
                                </a>
                            </div>
                            <a class="" href="<?php echo $articles['href']; ?>">
                                <?php if ($articles['date_added']) { ?>
                                    <span><?php echo $articles['date_added']; ?></span>
                                <?php } ?>
                                <p class="name-news">
                                    <?php if ($articles['name']) { ?>
                                        <?php echo $articles['name']; ?>
                                    <?php } ?>

                                </p>
                            </a>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <!--        </div>-->
            <!--    </div>-->
        </div>
    <?php } ?>

<?php } else { ?>
    <div class="article-content">
        <div class="wrapper-image_article">
            <?php if ($thumb) { ?>
                <a href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>" class="colorbox">
                    <img align="left" src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>"
                         alt="<?php echo $heading_title; ?>" id="image-article" />
                    <h1><?php echo $heading_title; ?></h1>
                </a>

            <?php } ?>
        </div>
        <div class="article-description">
            <?php echo $description; ?>
        </div>
        <?php if ($article_videos) { ?>
            <div class="content blog-videos">
                <?php foreach ($article_videos as $video) { ?>
                    <?php echo ($video['text']) ? '<h2 class="video-heading">'.$video['text'].'</h2>' : ''; ?>
                    <div>
                        <?php echo $video['code']; ?>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>

        <?php if ($author) { ?>
            <div class="content blog-author">
                <?php if ($author_image) { ?>
                    <img class="author-image" src="<?php echo $author_image; ?>" alt="<?php echo $author; ?>" />
                <?php } ?>
                <b><?php echo $text_posted_by; ?>
                    <a href="<?php echo $author_link; ?>"><?php echo $author; ?></a></b>
                <?php if ($author_desc) { ?>
                    <?php echo $author_desc; ?>
                <?php } ?>
            </div>
        <?php } ?>
    </div>
<?php } ?>

<?php if ($products) { ?>
<h3><?php echo $news_prelated; ?></h3>
<div class="row product-layout">
  <?php foreach ($products as $product) { ?>
  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="product-thumb transition">
      <div class="image"><a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" /></a></div>
      <div class="caption">
        <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
        <p><?php echo $product['description']; ?></p>
        <?php if ($product['rating']) { ?>
        <div class="rating">
          <?php for ($i = 1; $i <= 5; $i++) { ?>
          <?php if ($product['rating'] < $i) { ?>
          <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
          <?php } else { ?>
          <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
          <?php } ?>
          <?php } ?>
        </div>
        <?php } ?>
        <?php if ($product['price']) { ?>
        <p class="price">
          <?php if (!$product['special']) { ?>
          <?php echo $product['price']; ?>
          <?php } else { ?>
          <span class="price-new"><?php echo $product['special']; ?></span> <span class="price-old"><?php echo $product['price']; ?></span>
          <?php } ?>
          <?php if ($product['tax']) { ?>
          <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
          <?php } ?>
        </p>
        <?php } ?>
      </div>
      <div class="button-group">
        <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $button_cart; ?></span></button>
        <button type="button" data-toggle="tooltip" title="<?php echo $button_wishlist; ?>" onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-heart"></i></button>
        <button type="button" data-toggle="tooltip" title="<?php echo $button_compare; ?>" onclick="compare.add('<?php echo $product['product_id']; ?>');"><i class="fa fa-exchange"></i></button>
      </div>
    </div>
  </div>
  <?php } ?>
</div>
<?php } ?>

<?php if ($disqus_status) { ?>
<script type="text/javascript">
var disqus_shortname = '<?php echo $disqus_sname; ?>';
(function () {
var s = document.createElement('script'); s.async = true;
s.type = 'text/javascript';
s.src = 'http://' + disqus_shortname + '.disqus.com/count.js';
(document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
}());
</script>
<?php } ?>
<?php if ($acom != 0 && !$disqus_status && !$fbcom_status) { ?>
    <h2><a name="comments"></a><?php echo $title_comments; ?> <?php echo $text_coms; ?> "<?php echo $heading_title; ?>"</h2>
	<?php if ($comment) { ?>
    <?php foreach ($comment as $comment) { ?>
    <div class="content blog-content">
		<div  class="comment-header"><span class="comment-icon"></span><b><?php echo $comment['author']; ?></b> <?php echo $text_posted_on; ?> <?php echo $comment['date_added']; ?></div>
		<div class="comment-text"><?php echo $comment['text']; ?>
			<a onclick="addReply('<?php echo $comment['ncomment_id']; ?>', '<?php echo $comment['author']; ?>');"><?php echo $text_reply; ?></a>
		</div>
			<?php foreach ($comment['replies'] as $reply) { ?>
				<div class="content blog-reply">
					<div class="reply-header"><span class="comment-icon"></span><b><?php echo $reply['author']; ?></b> <?php echo $text_posted_on; ?> <?php echo $reply['date_added']; ?></div>
					<div class="comment-text"><?php echo $reply['text']; ?></div>
				</div>
			<?php } ?>
	</div>
    <?php } ?>
	<div class="row">
        <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
        <div class="col-sm-6 text-right"><?php echo $pag_results; ?></div>
	</div>
    <?php } ?>
    <div class="content" id="comment-form">
    <h2 id="com-title"><span class="blog-write"></span><?php echo $writec; ?></h2>
	<input type="hidden" name="reply_id" value="0" id="reply-id-field"/>
    <div class="comment-form">
	<div class="comment-left">
    <b><?php echo $entry_name; ?></b><br />
    <input class="form-control" type="text" name="name" value="<?php echo $customer_name; ?>" style="" />
    <div style="height: 5px; overflow: hidden">&nbsp;</div>
    <b><?php echo $entry_captcha; ?></b><br />
    <input class="form-control" type="text" name="captcha" style="" value="" />
	<div style="height: 5px; overflow: hidden">&nbsp;</div>
    <img src="index.php?route=tool/captcha" alt="" id="captcha" />
	</div>
	<div class="comment-right">
    <b><?php echo $entry_review; ?></b><br />
    <textarea class="form-control" name="text" cols="40" rows="4"></textarea>
    <span style="font-size: 11px;"><?php echo $text_note; ?></span>
	</div>
    </div>
    <div class="buttons">
      <div class="right">
		<button type="button" id="button-comment" data-loading-text="<?php echo $text_send; ?>" class="btn btn-primary"><?php echo $text_send; ?></button>
	  </div>
    </div>
  </div>
<?php } elseif ($acom != 0 && $disqus_status) { ?>
<div id="disqus_thread"></div>
<script type="text/javascript"><!--
var disqus_shortname = '<?php echo $disqus_sname; ?>';
var disqus_identifier = '<?php echo $disqus_id; ?>';
var disqus_url = '<?php echo $page_url; ?>';
/* * * DON'T EDIT BELOW THIS LINE * * */
(function() {
	var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
	dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
	(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
})();
--></script>
<noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
<?php } elseif ($acom != 0 && $fbcom_status) { ?>
<h2><a name="comments"></a><fb:comments-count href="<?php echo $page_url; ?>"></fb:comments-count> <?php echo $text_comments_to; ?> "<?php echo $heading_title; ?>"</h2>
<div class="fb-comments" data-width="100%" data-href="<?php echo $page_url; ?>" data-numposts="<?php echo $fbcom_posts; ?>" data-colorscheme="<?php echo $fbcom_theme; ?>"></div>
<script type="text/javascript">
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '<?php echo $fbcom_appid; ?>',
		  status     : true,
          xfbml      : true,
		  version    : 'v2.0'
        });
      };

      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
</script>
<?php } ?>
<script type="text/javascript"><!--
$('.colorbox').magnificPopup({ 
  type: 'image',
  gallery:{enabled:true},
  zoom: {
	enabled: true,
	duration: 300,
	opener: function(element) {
		return element.find('img');
	}
  }
});
//--></script>  
<script type="text/javascript"><!--
function addReply(reply_id, author) {
	$('#reply-id-field').attr('value', reply_id);
	$('#com-title').html("<span class=\"blog-write\"></span><?php echo $text_reply_to; ?> " + author + "<span onclick=\"cancelReply();\" title=\"Cancel Reply\" class=\"cancel-reply\"></span>");
	var scroll = $('#comment-form').offset();
	$('html, body').animate({ scrollTop: scroll.top - 80 }, 'slow');
}
function cancelReply(reply_id, author) {
	$('#reply-id-field').attr('value', 0);
	$('#com-title').html("<span class=\"blog-write\"></span><?php echo $writec; ?>");
}
$('#button-comment').bind('click', function() {
	$.ajax({
		type: 'POST',
		url: 'index.php?route=news/article/writecomment&news_id=<?php echo $news_id; ?>',
		dataType: 'json',
		data: 'name=' + encodeURIComponent($('input[name=\'name\']').val()) + '&text=' + encodeURIComponent($('textarea[name=\'text\']').val()) + '&captcha=' + encodeURIComponent($('input[name=\'captcha\']').val()) + '&reply_id=' + encodeURIComponent($('input[name=\'reply_id\']').val()),
		beforeSend: function() {
			$('.alert').remove();
			$('#button-comment').attr('disabled', true);
			$('#com-title').after('<div class="alert alert-danger ad1"><i class="fa fa-exclamation-circle"></i> <?php echo $text_wait; ?></div>');
		},
		complete: function() {
			$('#button-comment').attr('disabled', false);
			$('.ad1').remove();
		},
		success: function(data) {
			if (data.error) {
				$('#com-title').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + data.error + '</div>');
			}
			
			if (data.success) {
				$('#com-title').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + data.success + '</div>');
								
				$('input[name=\'name\']').val('');
				$('textarea[name=\'text\']').val('');
				$('input[name=\'captcha\']').val('');
			}
		}
	});
});
//--></script> 
<script type="text/javascript"><!--
	$(document).ready(function() {
		var articleImageWidth = $('img#image-article').width() + 30;
		var pageWidth = $('.article-content').width() * 0.65;
		if (articleImageWidth >= pageWidth) {
			$('img#image-article').attr("align","center");
			$('img#image-article').parent().addClass('centered-image');
		}
		$('img.article-image').each(function(index, element) {
		var articleWidth = $(this).parent().parent().width() * 0.7;
		var imageWidth = $(this).width() + 10;
		if (imageWidth >= articleWidth) {
			$(this).attr("align","center");
			$(this).parent().addClass('bigimagein');
		}
		});
		$(window).resize(function() {
		var articleImageWidth = $('img#image-article').width() + 30;
		var pageWidth = $('.article-content').width() * 0.65;
		if (articleImageWidth >= pageWidth) {
			$('img#image-article').attr("align","center");
			$('img#image-article').parent().addClass('centered-image');
		}
		$('img.article-image').each(function(index, element) {
		var articleWidth = $(this).parent().parent().width() * 0.7;
		var imageWidth = $(this).width() + 10;
		if (imageWidth >= articleWidth) {
			$(this).attr("align","center");
			$(this).parent().addClass('bigimagein');
		}
		});
		});
	});
//--></script> 