<?php if ($is_category) { ?>
  <?php if ($ncategories) { ?>
        <div class="category-list">
            <ul class="nav nav-tabs article-tabs">
                <?php $k = ''; foreach ($ncategories as $ncategory) { $k++; ?>
                    <li class="<?php if ($k == 1) { ?> active<?php } ?>"><a  href="#tab-article_<?php echo $k; ?>" data-toggle="tab"><?php echo $ncategory['name']; ?></a></li>
                <?php } ?>
            </ul>
            <div  class="news-content service-category row">
                <div class="tab-content">
                    <?php $k = ''; foreach ($ncategories as $ncategory) { $k++; ?>
                        <div class="tab-pane <?php if ($k == 1) { ?> active<?php } ?>" id="tab-article_<?php echo $k; ?>">
                            <?php foreach ($ncategory['article_news'] as $article_new) { ?>
                                <div class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="news-block js-height ">
                                        <div class="position-date">
                                            <a class="" href="<?php echo $article_new['href']; ?>">
                                                <?php if ($article_new['thumb']) { ?>
                                                    <img class="article-image" src="<?php echo $article_new['thumb']; ?>" title="<?php echo $article_new['name']; ?>" alt="<?php echo $article_new['name']; ?>" />
                                                <?php } ?>
                                            </a>
                                        </div>
                                        <a class="container-name" href="<?php echo $article_new['href']; ?>">
                                            <p class="name-news">
                                                <?php if ($article_new['name']) { ?>
                                                    <?php echo $article_new['name']; ?>
                                                <?php } ?>
                                            </p>
                                        </a>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
  <?php } ?>
<?php } ?>

<?php //} ?>
<?php if ($is_category) { ?>
  <?php if (!$ncategories && !$article) { ?>
  <div class="content"><?php echo $text_empty; ?></div>
  <?php } ?>
<?php } else { ?>
  <?php if (!$article) { ?>
  <div class="content"><?php echo $text_empty; ?></div>
  <?php } ?>
<?php } ?>
<?php if ($disqus_status) { ?>
<script type="text/javascript">
var disqus_shortname = '<?php echo $disqus_sname; ?>';
(function () {
var s = document.createElement('script'); s.async = true;
s.type = 'text/javascript';
s.src = 'http://' + disqus_shortname + '.disqus.com/count.js';
(document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
}());
</script>
<?php } ?>
<?php if ($fbcom_status) { ?>
<script type="text/javascript">
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '<?php echo $fbcom_appid; ?>',
		  status     : true,
          xfbml      : true,
		  version    : 'v2.0'
        });
      };

      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
</script>
<?php } ?>