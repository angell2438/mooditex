<?php echo $header; ?>
<?php if($ncat_info['ncategory_id'] == 63) { ?>
    <div class="container">
        <ul  class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
            <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
                <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                    <?php if($i+1<count($breadcrumbs)) { ?>
                        <a itemscope itemtype="http://schema.org/Thing" itemprop="item" href="<?php echo $breadcrumb['href']; ?>">
                            <span itemprop="name"><?php echo $breadcrumb['text']; ?></span></a>
                    <?php } else { ?><span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
                    <?php } ?>
                </li>
            <?php } ?>
        </ul>
    </div>
    <?php echo $description; ?>
<?php } else { ?>
    <div class="container">
        <ul  class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
            <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
                <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                    <?php if($i+1<count($breadcrumbs)) { ?>
                        <a itemscope itemtype="http://schema.org/Thing" itemprop="item" href="<?php echo $breadcrumb['href']; ?>">
                            <span itemprop="name"><?php echo $breadcrumb['text']; ?></span></a>
                    <?php } else { ?><span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
                    <?php } ?>
                </li>
            <?php } ?>
        </ul>
        <div class="row">
            <?php echo $column_left; ?>
            <?php if ($column_left && $column_right) { ?>
                <?php $class = 'col-sm-6'; ?>
            <?php } elseif ($column_left || $column_right) { ?>
                <?php $class = 'col-sm-9'; ?>
            <?php } else { ?>
                <?php $class = 'col-sm-12'; ?>
            <?php } ?>
            <div id="content" class="<?php echo $class; ?>">
                <?php echo $content_top; ?>
                <h1><?php echo $heading_title; ?></h1>
                <?php echo $description; ?>

                <?php echo $content_bottom; ?>
            </div>
            <?php echo $column_right; ?></div>
    </div>

<?php } ?>

<?php echo $footer; ?> 