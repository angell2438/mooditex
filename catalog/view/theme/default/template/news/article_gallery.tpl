<div class="photo_wideo_wrapp">

    <div class="tab-content">
        <?php if ($gallery_images) { ?>

                <div class="row">
                    <?php foreach ($gallery_images as $gallery) { ?>
                        <div class="col-sm-4 blog-gallery-layout">
                            <a class="" href="<?php echo $gallery['popup']; ?>" title="<?php echo $gallery['text']; ?>">
                                <img src="<?php echo $gallery['thumb']; ?>" alt="<?php echo $gallery['text']; ?>" />
                            </a>
                        </div>
                    <?php } ?>
                </div>
                <div class="photo_pagin">
                </div>

        <?php } ?>
        <?php if ($article_videos) { ?>

                <div class="row">
                    <?php foreach ($article_videos as $video) { ?>
                        <div class="col-sm-6 blog-videos_article">
                            <div class="wrapp_iframe">
                                <?php echo $video['code']; ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <div class="video_pagin">
                </div>

        <?php } ?>
    </div>
</div>
<script>
    $('.blog-gallery-layout').magnificPopup({
        type:'image',
        delegate: 'a',
        gallery: {
            enabled:true
        }
    });

    window.onpopstate = function() {
        var url = new URL(window.location.href);
        var page = url.searchParams.get("page");
        if(!page){
            $('.photo_pagin').find('li').eq(1).find('a').trigger('click');
        }else{
            $('.photo_pagin').find('li').eq(page).find('a').trigger('click');
        }
        history.replaceState();
    }

    $('.photo_pagin').pagination({
        items: <?php echo count($gallery_images); ?>,
        itemsOnPage: <?php echo $elem_limit; ?>,
        prevText: '<i class="fa fa-angle-left" aria-hidden="true"></i><?php echo $text_prev; ?>',
        nextText: '<?php echo $text_next; ?><i class="fa fa-angle-right" aria-hidden="true"></i>',
        onInit: function () {
            $('.photo_pagin .next').removeClass('next').parent().addClass('next');
            $('.photo_pagin .prev').removeClass('prev').parent().addClass('prev');
            $('.blog-gallery-layout').each(function (i, e) {
                if(i+1 > <?php echo $elem_limit; ?>){
                    $(e).hide();
                }
            });

//            console.log(window.location);
            var url = new URL(window.location.href);
            var page = url.searchParams.get("page");
//            console.log(url);
            if(page){
                $('.photo_pagin').find('li').eq(page).find('a').trigger('click');
            }
        },
        onPageClick: function (pN, e) {
            $('.photo_pagin .next').removeClass('next').parent().addClass('next');
            $('.photo_pagin .prev').removeClass('prev').parent().addClass('prev');
            $('.blog-gallery-layout').show();
            $('.blog-gallery-layout').each(function (i, el) {
                if((i+1 <= pN*<?php echo $elem_limit; ?>-<?php echo $elem_limit; ?>) || (i+1 > pN*<?php echo $elem_limit; ?>)){
                    $(el).hide();
                }
            });

            var url = new URL(window.location.href);
            var news_id = url.searchParams.get("news_id");
            history.pushState('', "", "index.php?route=news/article&news_id="+news_id+"&page="+pN);

            e.preventDefault();
        }
    });

    $('.video_pagin').pagination({
        items: <?php echo count($article_videos); ?>,
        itemsOnPage: <?php echo $elem_limit; ?>,
        prevText: '<i class="fa fa-angle-left" aria-hidden="true"></i><?php echo $text_prev; ?>',
        nextText: '<?php echo $text_next; ?><i class="fa fa-angle-right" aria-hidden="true"></i>',
        onInit: function () {
            $('.video_pagin .next').removeClass('next').parent().addClass('next');
            $('.video_pagin .prev').removeClass('prev').parent().addClass('prev');
            $('.blog-videos_article').each(function (i, e) {
                if(i+1 > <?php echo $elem_limit; ?>){
                    $(e).hide();
                }
            });
        },
        onPageClick: function (pN, e) {
            $('.video_pagin .next').removeClass('next').parent().addClass('next');
            $('.video_pagin .prev').removeClass('prev').parent().addClass('prev');
            $('.blog-videos_article').show();
            $('.blog-videos_article').each(function (i, el) {
                if((i+1 <= pN*<?php echo $elem_limit; ?>-<?php echo $elem_limit; ?>) || (i+1 > pN*<?php echo $elem_limit; ?>)){
                    $(el).hide();
                }
            });
            e.preventDefault();
        }
    });

    $('.photo_pagin a').click(function (e) {
        e.preventDefault();
    })
</script>