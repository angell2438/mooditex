<div class="container-product">
    <h2><?php echo $heading_title; ?></h2>
    <div class="row">
        <?php foreach ($products as $product) { ?>
            <div class="product-layout col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="product-thumb transition">
                    <div class="image">
                        <a href="<?php echo $product['href']; ?>">
                            <img src="<?php echo $product['thumb']; ?>"
                                 alt="<?php echo $product['name']; ?>"
                                 title="<?php echo $product['name']; ?>" class="img-responsive" />
                        </a>
                    </div>
                    <div class="caption">
                        <a href="<?php echo $product['href']; ?>">
                            <?php echo $product['name']; ?>
                            <?php if ($product['price']) { ?>
                                <p class="price">
                                    <?php if (!$product['special']) { ?>
                                        <span><?php echo $product['price']; ?></span>
                                    <?php } else { ?>
                                        <span class="price-new"><?php echo $product['special']; ?></span>
                                        <span class="price-old"><?php echo $product['price']; ?></span>
                                    <?php } ?>
                                </p>
                            <?php } ?>
                        </a>

                    </div>
                </div>
            </div>
        <?php } ?>
    </div>

</div>
