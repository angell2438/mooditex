<?php if ($categories) { ?>
    <h2><?php echo $heading_title; ?></h2>
    <div class="wrapper-category_home">

        <div class="category-slider">
            <?php foreach ($categories as $category) { ?>
                <?php if ($category['children']) { ?>
                    <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
                        <?php foreach ($children as $child) { ?>
                            <div class="item">
                                <a href="<?php echo $child['href']; ?>" >
                                    <img src="<?php echo $child['thumb']; ?>" alt="<?php echo $child['name']; ?>">
                                    <?php echo $child['name']; ?>
                                </a>
                            </div>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
            <?php } ?>
        </div>
    </div>
<?php } ?>