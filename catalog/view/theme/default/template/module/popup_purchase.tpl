<div class="modal fade modal-buy-one-click" id="buy-one-click" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" id="popup-purchase-wrapper" role="document">
        <div class="modal-content">
            
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="icon-close" aria-hidden="true"></span>
                </button>
                <h2><?php echo $heading_title;?></h2>

            <div class="modal-body">
                <div class="popup-center">
                    <?php if ($stock_warning) { ?>
                        <div class="alert alert-danger"><i
                                    class="fa fa-exclamation-circle"></i> <?php echo $stock_warning; ?>
                        </div>
                    <?php } ?>
                    <?php if ($minimum > 1) { ?>
                        <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $text_minimum; ?>
<!--                            <button type="button" class="close" data-dismiss="alert">&times;</button>-->
                        </div>
                    <?php } ?>
                    <?php if (!$stock_warning) { ?>
                        <form method="post" enctype="multipart/form-data" id="purchase-form">

                            <div class="payment-info">

                                <?php if ($popup_purchase_data['firstname']) { ?>
                                    <div>
                                        <input class="form-control" name="firstname" value="<?php echo $firstname; ?>"
                                               placeholder="<?php echo $enter_firstname; ?>"/>
                                    </div>
                                <?php } ?>
                                <?php if ($popup_purchase_data['telephone']) { ?>
                                    <div>
                                        <input class="form-control" name="telephone" type="tel" value="<?php echo $telephone; ?>"
                                               placeholder="<?php echo $enter_telephone; ?>"/>
                                    </div>
                                <?php } ?>
                                <?php if ($popup_purchase_data['email']) { ?>
                                    <div>
                                        <input class="form-control" name="email" value="<?php echo $email; ?>"
                                               placeholder="<?php echo $enter_email; ?>"/>
                                    </div>
                                <?php } ?>
                                <?php if ($popup_purchase_data['comment']) { ?>
                                    <div>
                                        <textarea class="form-control" name="comment" placeholder="<?php echo $enter_comment; ?>"><?php echo $comment;?></textarea>
                                    </div>
                                <?php } ?>
                            </div>
                            <div class="right">
                                <div class="product-name"><?php echo $product_name; ?></div>
                                <input name="product_id" value="<?php echo $product_id; ?>" style="display: none;"
                                       type="hidden"/>
                                <div class="option-wrapper">
                                    <?php if ($options) { ?>
                                        <?php foreach ($options as $option) { ?>
                                            <?php if ($option['type'] == 'select') { ?>
                                                <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                    <label class="control-label" for="pop_input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                                    <select name="option[<?php echo $option['product_option_id']; ?>]" id="pop_input-option<?php echo $option['product_option_id']; ?>" class="form-control">
                                                        <option value=""><?php echo $text_select; ?></option>
                                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                            <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                                                                <?php if ($option_value['price']) { ?>
                                                                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                                <?php } ?>
                                                            </option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            <?php } ?>
                                            <?php if ($option['type'] == 'radio') { ?>
                                                <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                    <label class="control-label"><?php echo $option['name']; ?></label>
                                                    <div id="pop_input-option<?php echo $option['product_option_id']; ?>" class="opt_wrapp size_opt">
                                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                            <div class="radio ">
                                                                <input id="pop_optionp-<?php echo $option_value['product_option_value_id']; ?>" type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                                                                <label for="pop_optionp-<?php echo $option_value['product_option_value_id']; ?>">
                                                                    <?php echo $option_value['name']; ?>
                                                                    <?php if ($option_value['price']) { ?>
                                                                        (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                                    <?php } ?>
                                                                </label>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <?php if ($option['type'] == 'checkbox') { ?>
                                                <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                    <label class="control-label"><?php echo $option['name']; ?></label>
                                                    <div id="pop_input-option<?php echo $option['product_option_id']; ?>">
                                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                                                                    <?php echo $option_value['name']; ?>
                                                                    <?php if ($option_value['price']) { ?>
                                                                        (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                                    <?php } ?>
                                                                </label>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <?php if ($option['type'] == 'image') { ?>
                                                <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                    <label class="control-label"><?php echo $option['name']; ?>:</label>
                                                    <div id="pop_input-option<?php echo $option['product_option_id']; ?>" class="opt_wrapp size_opt">
                                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                            <div class="radio radio-image">
                                                                <input id="pop_optionp-<?php echo $option_value['product_option_value_id']; ?>" type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                                                                <label for="pop_optionp-<?php echo $option_value['product_option_value_id']; ?>">
                                                                    <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" />
                                                                    <?php if ($option_value['price']) { ?>
                                                                        (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                                    <?php } ?>
                                                                </label>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <?php if ($option['type'] == 'text') { ?>
                                                <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                    <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                                    <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                                </div>
                                            <?php } ?>
                                            <?php if ($option['type'] == 'textarea') { ?>
                                                <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                    <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                                    <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"><?php echo $option['value']; ?></textarea>
                                                </div>
                                            <?php } ?>
                                            <?php if ($option['type'] == 'file') { ?>
                                                <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                    <label class="control-label"><?php echo $option['name']; ?></label>
                                                    <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default btn-block"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
                                                    <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>" />
                                                </div>
                                            <?php } ?>
                                            <?php if ($option['type'] == 'date') { ?>
                                                <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                    <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                                    <div class="input-group date">
                                                        <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                                        <span class="input-group-btn">
                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                </span></div>
                                                </div>
                                            <?php } ?>
                                            <?php if ($option['type'] == 'datetime') { ?>
                                                <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                    <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                                    <div class="input-group datetime">
                                                        <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                                        <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
                                                </div>
                                            <?php } ?>
                                            <?php if ($option['type'] == 'time') { ?>
                                                <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                    <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                                    <div class="input-group time">
                                                        <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                                        <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
                                                </div>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                                <?php if ($price) { ?>
                                    <div class="product-price">
                                        <div class="quantity_wrapp">
                                            <?php if ($popup_purchase_data['quantity']) { ?>
                                                <div class="payment-quantity">
                                                    <div class="number">

                                                        <div class="frame-change-count">
                                                            <div class="btn-minus">
                                                                <button type="button"
                                                                        onclick="$(this).parent().next().val(~~$(this).parent().next().val()-1); update_prices('<?php echo $product_id; ?>');">
                                                                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                                                                </button>
                                                            </div>
                                                            <input type="text" name="quantity" value="<?php echo $minimum; ?>" maxlength="3"
                                                                   class="form-control"
                                                                   onchange="update_prices( '<?php echo $product_id; ?>' ); return validate(this);"
                                                                   onkeyup="update_prices( '<?php echo $product_id; ?>' ); return validate(this);"/>
                                                            <div class="btn-plus">
                                                                <button type="button"
                                                                        onclick="$(this).parent().prev().val(~~$(this).parent().prev().val()+1); update_prices('<?php echo $product_id; ?>');">
                                                                    <i class="fa fa-angle-up" aria-hidden="true"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } else { ?>
                                                <input type="hidden" name="quantity" value="<?php echo $minimum; ?>"/>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php } ?>

                            </div>
                        </form>
                    <?php } ?>

                </div>

            </div>
                <?php if (!$stock_warning) { ?>
                    <a id="popup-checkout-button" class="btn btn-default"><span><?php echo $button_checkout; ?></span></a>
                <?php } ?>

            <?php if (!$stock_warning) { ?>
                <script type="text/javascript"><!--
                    $(document).on('click', 'button[id^=\'button-upload\']', function () {
                        var node = this;

                        $('#form-upload').remove();

                        $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

                        $('#form-upload input[name=\'file\']').trigger('click');

                        if (typeof timer != 'undefined') {
                            clearInterval(timer);
                        }

                        timer = setInterval(function () {
                            if ($('#form-upload input[name=\'file\']').val() != '') {
                                clearInterval(timer);

                                $.ajax({
                                    url: 'index.php?route=tool/upload',
                                    type: 'post',
                                    dataType: 'json',
                                    data: new FormData($('#form-upload')[0]),
                                    cache: false,
                                    contentType: false,
                                    processData: false,
                                    beforeSend: function () {
                                        $(node).button('loading');
                                    },
                                    complete: function () {
                                        $(node).button('reset');
                                    },
                                    success: function (json) {
                                        $('.text-danger').remove();

                                        if (json['error']) {
                                            $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
                                        }

                                        if (json['success']) {
                                            alert(json['success']);

                                            $(node).parent().find('input').attr('value', json['code']);
                                        }
                                    },
                                    error: function (xhr, ajaxOptions, thrownError) {
                                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                                    }
                                });
                            }
                        }, 500);
                    });
                    //--></script>
                <script type="text/javascript"><!--
                    $(document).ready(function(){
                        $('#popup-purchase-wrapper input[type=tel]').mask('+38(999) 999 99 99');


                        $('#popup-purchase-wrapper input').focus(function () {
                            $(this).removeClass('error_style');
                            $(this).parent().find('.text-danger').remove();
                        });
                    });
                    function masked(element, status) {
                        if (status == true) {
                            $('<div/>')
                                .attr({'class': 'masked'})
                                .prependTo(element);
                            $('<div class="masked_loading" />').insertAfter($('.masked'));
                        } else {
                            $('.masked').remove();
                            $('.masked_loading').remove();
                        }
                    }

                    <?php if ($popup_purchase_data['quantity']) { ?>
                    function validate(input) {
                        input.value = input.value.replace(/[^\d,]/g, '');
                    }
                    <?php } ?>

                    $('#popup-checkout-button').on('click', function () {
                        masked('#popup-purchase-wrapper', true);
                        $.ajax({
                            type: 'post',
                            url: 'index.php?route=module/popup_purchase/make_order',
                            dataType: 'json',
                            data: $('#purchase-form, .quantity_wrapp input[name=quantity]').serialize(),
                            success: function (json) {

                                if (json['error']) {
                                    if (json['error']['field']) {
                                        masked('#popup-purchase-wrapper', false);
                                        $('.text-danger').remove();
                                        $.each(json['error']['field'], function (i, val) {
                                            $('[name="' + i + '"]').addClass('error_style').after('<div class="text-danger">' + val + '</div>');
                                        });
                                    }

                                    if (json['error']['option']) {
                                        for (i in json['error']['option']) {
                                            var element = $('#pop_input-option' + i.replace('_', '-'));

                                            if (element.parent().hasClass('input-group')) {
                                                element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                                            } else {
                                                element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                                            }
                                        }
                                    }

                                    if (json['error']['recurring']) {
                                        $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
                                    }
                                    $('.text-danger').parent().addClass('has-error');
                                } else {
                                    if (json['output']) {


                                        masked('#popup-purchase-wrapper', false);
                                        $('#popup-checkout-button').remove();
                                        $('#popup-purchase-wrapper .popup-center').html(json['output']);

                                    }
                                }
                            }
                        });
                    });

                    <?php if ($popup_purchase_data['quantity']) { ?>
                    function update_prices(product_id) {
                        masked('#popup-purchase-wrapper', true);
                        var input_val = $('.quantity_wrapp').find('input[name=quantity]').val();
                        var quantity = parseInt(input_val);

                        <?php if ($minimum > 1) { ?>
                        if (quantity < <?php echo $minimum; ?>) {
                            quantity = $('.quantity_wrapp').find('input[name=quantity]').val(<?php echo $minimum; ?>);
                            masked('#popup-purchase-wrapper', false);
                            return;
                        }
                        <?php } else { ?>
                        if (quantity == 0) {
                            quantity = $('.quantity_wrapp').find('input[name=quantity]').val(1);
                            masked('#popup-purchase-wrapper', false);
                            return;
                        }
                        <?php } ?>

                        $.ajax({
                            url: 'index.php?route=module/popup_purchase/update_prices&product_id=' + product_id + '&quantity=' + quantity,
                            type: 'post',
                            dataType: 'json',
                            data: $('#purchase-form').serialize(),
                            success: function (json) {
                                $('#main-price').html(json['price']);
                                $('#special-price').html(json['special']);
                                $('#main-tax').html(json['tax']);
                                masked('#popup-purchase-wrapper', false);
                            }
                        });
                    }
                    <?php } ?>

                    $('select[name=\'recurring_id\'], input[name="quantity"]').change(function () {
                        $.ajax({
                            url: 'index.php?route=product/product/getRecurringDescription',
                            type: 'post',
                            data: $('#purchase-form input[name=\'product_id\'], #purchase-form input[name=\'quantity\'], #purchase-form select[name=\'recurring_id\']'),
                            dataType: 'json',
                            beforeSend: function () {
                                $('#recurring-description').html('');
                            },
                            success: function (json) {
                                $('.alert, .text-danger').remove();

                                if (json['success']) {
                                    $('#recurring-description').html(json['success']);
                                }
                            }
                        });
                    });
                    //--></script>
            <?php } ?>
            <style type="text/css">
                <?php if ($popup_purchase_data['color_h1']) { ?>
                #popup-purchase-wrapper .product-name {
                    color: <?php echo $popup_purchase_data['color_h1']; ?>;
                }

                <?php } ?>
                <?php if ($popup_purchase_data['color_price']) { ?>
                #popup-purchase-wrapper #main-price {
                    color: <?php echo $popup_purchase_data['color_price']; ?>;
                }

                <?php } ?>
                <?php if ($popup_purchase_data['color_special_price']) { ?>
                #popup-purchase-wrapper #special-price {
                    color: <?php echo $popup_purchase_data['color_special_price']; ?>;
                }

                <?php } ?>
                <?php if ($popup_purchase_data['color_description']) { ?>
                #popup-purchase-wrapper .product-description {
                    color: <?php echo $popup_purchase_data['color_description']; ?>;
                }

                <?php } ?>
                <?php if ($popup_purchase_data['color_checkout_button']) { ?>
                #popup-purchase-wrapper .popup-footer a {
                    color: <?php echo $popup_purchase_data['color_checkout_button']; ?>;
                }

                <?php } ?>
                <?php if ($popup_purchase_data['color_close_button']) { ?>
                #popup-purchase-wrapper .popup-footer button {
                    color: <?php echo $popup_purchase_data['color_close_button']; ?>;
                }

                <?php } ?>
                <?php if ($popup_purchase_data['background_checkout_button']) { ?>
                #popup-purchase-wrapper .popup-footer a {
                    background: <?php echo $popup_purchase_data['background_checkout_button']; ?>;
                }

                <?php } ?>
                <?php if ($popup_purchase_data['background_close_button']) { ?>
                #popup-purchase-wrapper .popup-footer button {
                    background: <?php echo $popup_purchase_data['background_close_button']; ?>;
                }

                <?php } ?>
                <?php if ($popup_purchase_data['background_checkout_button_hover']) { ?>
                #popup-purchase-wrapper .popup-footer a:hover {
                    background: <?php echo $popup_purchase_data['background_checkout_button_hover']; ?>;
                }

                <?php } ?>
                <?php if ($popup_purchase_data['background_close_button_hover']) { ?>
                #popup-purchase-wrapper .popup-footer button:hover {
                    background: <?php echo $popup_purchase_data['background_close_button_hover']; ?>;
                }

                <?php } ?>
                <?php if ($popup_purchase_data['border_checkout_button']) { ?>
                #popup-purchase-wrapper .popup-footer a {
                    border-color: <?php echo $popup_purchase_data['border_checkout_button']; ?>;
                }

                <?php } ?>
                <?php if ($popup_purchase_data['border_close_button']) { ?>
                #popup-purchase-wrapper .popup-footer button {
                    border-color: <?php echo $popup_purchase_data['border_close_button']; ?>;
                }

                <?php } ?>
                <?php if ($popup_purchase_data['border_checkout_button_hover']) { ?>
                #popup-purchase-wrapper .popup-footer a:hover {
                    border-color: <?php echo $popup_purchase_data['border_checkout_button_hover']; ?>;
                }

                <?php } ?>
                <?php if ($popup_purchase_data['border_close_button_hover']) { ?>
                #popup-purchase-wrapper .popup-footer button:hover {
                    border-color: <?php echo $popup_purchase_data['border_close_button_hover']; ?>;
                }

                <?php } ?>
            </style>
        </div>
    </div>
</div>
