<div class="big-slider">
    <div id="slideshow<?php echo $module; ?>" class="wrapper_big-slider" style="opacity: 1;">
        <?php foreach ($banners as $banner) { ?>
            <div class="item">
                <?php if ($banner['link']) { ?>
                    <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" />
                    <?php if ($banner['title']) { ?>
                        <div class="text-info">
                            <div class="banner_title"><?php echo $banner['title']; ?></div>
                            <a class="btn-default btn-white" href="<?php echo $banner['link']; ?>" ><span><?php echo $btn_more; ?></span></a>
                        </div>
                    <?php } else { ?>

                    <?php } ?>

                <?php } else { ?>
                    <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" />
                    <?php if ($banner['title']) { ?>
                        <div class="text-info">
                            <div class="banner_title"><?php echo $banner['title']; ?></div>
                        </div>
                    <?php } else { ?>

                    <?php } ?>
                <?php } ?>
            </div>
        <?php } ?>
    </div>

</div>

