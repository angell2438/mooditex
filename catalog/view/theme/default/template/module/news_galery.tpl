<div class="wrapper-news galery-block">
    <h2><?php echo $heading_title; ?></h2>
    <div  class="news-content">
        <div  class="galery-list">
                <?php foreach ($article as $articles) { ?>
                    <div class="galery-items">
                        <div class="position-date">
                            <a class="" href="<?php echo $articles['href']; ?>">
                                <?php if ($articles['thumb']) { ?>
                                    <img class="article-image" src="<?php echo $articles['thumb']; ?>" title="<?php echo $articles['name']; ?>" alt="<?php echo $articles['name']; ?>" />
                                <?php } ?>
                            </a>
                        </div>
                    </div>
                <?php } ?>
        </div>
        <a class="btn-default" href="<?php echo $newslink; ?>"><span><?php echo $text_more_news; ?></span></a>
    </div>
</div>

