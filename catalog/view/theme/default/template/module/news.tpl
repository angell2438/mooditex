<div class="wrapper-news">
    <div class="container">
        <h2><?php echo $heading_title; ?></h2>
        <div  class="news-content">
            <div id='news-item' class="news-list">
                <div class="row">
                    <?php foreach ($article as $articles) { ?>
                        <div class="news-block js-height col-md-4 col-sm-6 col-xs-12">
                            <div class="position-date">
                                <a class="" href="<?php echo $articles['href']; ?>">
                                    <?php if ($articles['thumb']) { ?>
                                        <img class="article-image" src="<?php echo $articles['thumb']; ?>" title="<?php echo $articles['name']; ?>" alt="<?php echo $articles['name']; ?>" />
                                    <?php } ?>
                                </a>
                            </div>
                            <a class="" href="<?php echo $articles['href']; ?>">
                                <?php if ($articles['date_added']) { ?>
                                    <span><?php echo $articles['date_added']; ?></span>
                                <?php } ?>
                                <p class="name-news">
                                    <?php if ($articles['name']) { ?>
                                        <?php echo $articles['name']; ?>
                                    <?php } ?>

                                </p>
                            </a>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <a class="btn-default" href="<?php echo $newslink; ?>"><span><?php echo $text_more_news; ?></span></a>
        </div>
    </div>
</div>

