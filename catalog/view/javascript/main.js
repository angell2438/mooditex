function searchwidth() {
    var ml = $('.flex-wrapper.flex-right').width();
    aw = ml + -60;
    $('#search .search_block_content').css("width", aw);
    $('#search .search_block_content').css("left", -aw);
}
(function() {
    var element = $('.letter-svg');

    var scene = $('#Page-1');

    var buildings = [];

    var timeBeteweenBuldings = 0.175;

    var buildingAnimationMaxTime = 400;
    var buildingAnimationMinTime = 300;

    var buildingsNames = [{
        name: '#building1'
    }, {
        name: '#building2'
    }, {
        name: '#building3'
    }, {
        name: '#building4'
    }, {
        name: '#building5'
    }, {
        name: '#building6'
    }, {
        name: '#building7'
    }, {
        name: '#building8'
    }, {
        name: '#building9'
    }, {
        name: '#building10'
    }];

    $(document).ready(init);
    $(window).load(startAnimation);

    function init() {
        initBuildings();
        hideAllElements();
    }

    function hideAllElements() {
        buildings.forEach(hideBuilding);
    }

    function initBuildings() {
        buildingsNames = shuffle(buildingsNames);
        buildingsNames.forEach(findBuildings);
    }

    function findBuildings(building, index) {
        var object = scene.find(building.name);
        populateBuildings(object, index);
    }

    function populateBuildings(building, index) {
        buildings.push($(building));
    }

    function hideBuilding(building) {
        TweenLite.set(building, { scaleY: 0, transformOrigin: '100% 100%' });
    }

    function startAnimation() {
        element.css('visibility', 'visible');
        buildings.forEach(showBuildings);

    }

    function showBuildings(building, index) {
        TweenLite.to(
            building,
            getRandomAnimationTime(buildingAnimationMinTime, buildingAnimationMaxTime),
            {
                delay: timeBeteweenBuldings + timeBeteweenBuldings * index,
                scaleY: 1,
                ease: Back.easeOut.config(1.7)
            }
        );
    }

    function getRandomAnimationTime(min, max) {
        return (Math.floor(Math.random() * (max - min)) + min) / 1000;
    }

    function shuffle(array) {
        shuffledArray = array;
        for (
            var j, x, i = shuffledArray.length; i;
            j = Math.floor(Math.random() * i),
                x = shuffledArray[--i],
                shuffledArray[i] = shuffledArray[j],
                shuffledArray[j] = x
        );
        return shuffledArray;
    }
})();
// функция узнает размер окна браузера, и задает её для блока div
function fullHeight(){
    $('.error-wrapper').css({
        height: $(window).height() + 'px'
    });
}

// задаем высоту при первичной загрузке
fullHeight();

// высота при изменении размера окна браузера
$(window).resize( fullHeight );
$(window).load( function() {

    $(".ninja-btn").click( function() {
        $(".ninja-btn, .panel-overlay, .panel").toggleClass("active");
        /* Check panel overlay */
        if ($(".panel-overlay").hasClass("active")) {
            $(".panel-overlay").addClass('active-overlay');
        } else {
            $(".panel-overlay").removeClass('active-overlay');
        }
    });

});
$(window).on("load resize", function() {
    var menuHeightOffset = $(".panel").find("ul").height() /2;

    $(".panel").find("ul").css({
        "margin-top": -menuHeightOffset
    });
    $('select.form-control').select2({
        minimumResultsForSearch: Infinity
    });
    setTimeout(function() {
        $(".alert").alert('close');
    }, 5000);
    // if ($(window).width() <= 991) {
    //     $('header .js-position_menu').insertAfter('.js-container');
    //     $('.js-position_footer').insertAfter('.info-shops');
    // } else{
    //     $('header .js-position_menu').insertBefore('.js-container');
    //     $('.js-position_footer').insertBefore('.newsletter');
    // }
    if ($(window).width() <= 767) {
        $('.wrapper-bascat').insertAfter('.float-left');
        $('.col-apteka-product').toggleClass('mini-apteka-items');
        $('.col-apteka-info').on('click', function () {
            $(this).parent().parent().toggleClass('open-aptek');
        });
        $('.wish-btn').removeAttr("data-toggle");
        $('.wish-btn').removeAttr("data-original-title");
        $('.wish-btn').removeAttr("title");
        $('.product-thumb .image p').dotdotdot({
            height: 30
        });
    } else{
        $('.wrapper-bascat').insertAfter('.js-position-bascat');
        $('.col-apteka-product').removeClass('mini-apteka-items');

    }
});
var sld = function() {
    if ($(window).width() <= 991) {
        $('.category-slider').slick({
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 2,
            slidesToScroll: 1,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        infinite: true,
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        arrows: false
                    }

                },
                {
                    breakpoint: 680,
                    settings: {
                        infinite: true,
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: false
                    }

                },
                {
                    breakpoint: 550,
                    settings: {
                        infinite: true,
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: false
                    }

                }
            ]
        });
    } else {
        $('.category-slider').slick({
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 4,
            slidesToScroll: 1
        });
    }
};
sld();

$(window).resize(sld);

function productImageSlider() {
    if ($('*').is('.thumbnails')) {

        $('.thumbnails').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            fade: true,
            asNavFor: '.thumbnails-mini'
        });
        $('.thumbnails-mini').slick({
            slidesToShow: 4,
            slidesToScroll: 1,
            asNavFor: '.thumbnails',
            dots: false,
            speed: 200,
            centerMode: true,
            vertical: false,
            focusOnSelect: true,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        vertical: false,
                        arrows: false,
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                }
            ]
        });
        if ($(window).width() > 767) {
            $('.thumbnails').on('afterChange', function (event, slick, currentSlide) {
                $(".thumbnails .slick-current img").elevateZoom({
                    zoomType: "lens",
                    lensShape: "square",
                    lensSize: 160,
                    borderSize: 0
                });
            });
            $(".thumbnails .slick-current img").elevateZoom({
                zoomType: "lens",
                lensShape: "square",
                lensSize: 160,
                borderSize: 0
            });
        }
        $('#lightgallery').lightGallery({
            loop: true,
            fourceAutoply: true,
            thumbnail: true,
            hash: false,
            speed: 400,
            scale: 1,
            keypress: true,
            counter: false,
            download: false,
        });
        $('.click-triger-zoom').on('click', function (e) {
            $('.thumbnails .slick-current img').data("link");
            $('#' + $('.thumbnails .slick-current img').data("link")).trigger("click");
        });
    }
}
$(document).ready(function () {
    // btnBuble();
    searchwidth();
    productImageSlider();
    $(window).bind("resize", function() {
        searchwidth();
    });
    if ($(window).width() <= 991) {
        $('.wrapper-info .row').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            infinite: true,
            dots: false,
            prevArrow: '<button type="button" class="products_row-prev_btn products_row-btn"><i class="icon-right-arrow" aria-hidden="true"></i></button>',
            nextArrow: '<button type="button" class="products_row-next_btn products_row-btn"><i class="icon-right-arrow" aria-hidden="true"></i></button>',
            initialSlide: 0
        });
        $('.container-product .row').slick({
            slidesToShow: 2,
            slidesToScroll: 1,
            centerMode: true,
            arrows: false,
            infinite: true,
            dots: false,
            initialSlide: 0,
            responsive: [
                {
                    breakpoint: 580,
                    settings: {
                        infinite: true,
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });

        // $('header .js-position_menu').insertAfter('.js-container');
    } else{
        // $('header .js-position_menu').insertBefore('.js-container');
    }
    $('.btn-callphone').on('click', function() {
        // console.log($(this));
        $(this).parent().toggleClass('open');
        $(this).parent().find('.js-visible').toggleClass('open');
    });
    $('.box-heading').click(function () {
        $(this).toggleClass('open');
        $('.mfilter-content').toggle();
    });
    $(".js-modal-btn").modalVideo();

    var isActive = false;

    $('.ninja-btn').on('click', function() {
        if (isActive) {
            $(this).removeClass('active');
            $('body').removeClass('menu-open');
        } else {
            $(this).addClass('active');
            $('body').addClass('menu-open');
        }
        isActive = !isActive;
    });

    $('.share_wrapp .text').click(function() {
        $(this).parent().toggleClass('active');
    });
    $('.mfilter-box .box-heading').click(function () {
        $(this).toggleClass('open');
        $('.mfilter-content').toggle();
    });
    $(".footer-title").click(function(e){
        e.preventDefault();
        if ($(window).width() <= 768) {
            var $parentBlock = $(this).parent('.parent-elements');
            console.log($parentBlock);
            $parentBlock.toggleClass("open-list");
            if($parentBlock.hasClass("open-list")){
                $(".open-list").find('.list-unstyled').slideDown(500);
            } else {
                $(".list-unstyled").slideUp(500);
            }
            $(this).toggleClass("open");
        } else {
        }
    });

    $('body').on('click', function() {
        $('.search_container').removeClass('open');
    });
    $('.search_container').on('click', function(e) {
        e.stopPropagation();
    });
    $('.search_container .header_search').on('click', function(e) {
        e.stopPropagation();
        $('.search_container').toggleClass('open');
    });
    $('.header_search').mouseover(function() {
        $('.search_container').addClass('open');
    });
    $('#input-search').mouseleave(function() {
        if (!$('#input-search').is(':focus')) {
            $('.search_container').removeClass('open');
        }
    });
    $('.search_container').mouseleave(function() {
        if (!$('#input-search').is(':focus')) {
            $('.search_container').removeClass('open');
        }
    });
    $(".slick-slider").on('init', function(event, slick) {
        $(".slick-slider").css("opacity", "1");
    });

    $('.js-toggle-main-menu').click(function(){
        $(this).toggleClass('active');
    });
    $(".filter-sm").click(function(e){
        e.preventDefault();
        if ($(window).width() <= 991) {
            $("#column-left").toggleClass("open-filter");
            $(this).toggleClass("open");
        } else {
        }
    });
    $(".wish-btn").on('click', function(){
        $(this).addClass('active').prop('disabled', true);
        $(this).html('<i class="fa fa-heart" aria-hidden="true"></i>');

    });

    $(".wish-prod").on('click', function(){
        $(this).addClass('active').prop('disabled', true);
        $(this).html('<i class="fa fa-star" aria-hidden="true"></i> В закладках');
    });
    var url = document.location.href;
    $.each($(".active-menu li a"), function() {
        if (this.href == url) {
            $(this).parent().addClass('activeCSS');
        };
    });
    $('.menuupd-block').hover(
        function() {
            $('.bluebutton ').addClass('class-name');

        }, function() {
            $('.bluebutton ').removeClass('class-name');
        }
    );
    // $("#buy-one-click").on('hidden.bs.modal', function () {
    //     $(this).data('bs.modal', null);
    // });
    $('.modal-header .close').on('click', function (e) {
        console.log("Modal hidden");
        $("#buy-one-click").html("");
    });
    $('body').on('click','.modal-header .close',function() {
        console.log("Modal hidden");
        $('#buy-one-click').modal('hide').remove();
        $('.modal-backdrop').remove();
    });

    $(document).mouseup(function (e){ // событие клика по веб-документу
        var div = $("#buy-one-click"); // тут указываем ID элемента
        if (!div.is(e.target) // если клик был не по нашему блоку
            && div.has(e.target).length === 0) { // и не по его дочерним элементам
            $('#buy-one-click').modal('hide').remove();
            $('.modal-backdrop').remove();
        }
    });
    // $("#buy-one-click").on('.close', function () {
    //     $(this).data('bs.modal', null);
    // });
    // $('#buy-one-click').on('hidden', function(){
    //     $(this).data('modal', null);
    // });
    $('.click_price').on('click', function(e){
        e.preventDefault();
        $('.tab-pane').removeClass('active');
        $('.nav-tabs').find('li').removeClass('active');
        $('.nav-tabs').find('li').first().addClass('active');
        $('#tab-specification').toggleClass('active');
        $('a.click_price').toggleClass('open');
        var elementClick = $(this).attr("href");
        var destination = $(elementClick).offset().top;
        if ($.browser.safari) {
            $('.page-wrapper').animate({ scrollTop: destination }, 1100); //1100 - скорость
        } else {
            $('.page-wrapper').animate({ scrollTop: destination }, 1100);
        }
        return false;
    });


    $('.open_comment').on('click', function(){
        $('.comment-block').toggleClass('open');
    });

    $('button.in_wishlist').click(function () {
        $(this).attr('disabled', true);
    });

    var countLi = $('.mfilter-content ul li').size();
    if(countLi > 2){
        $('.mfilter-openall a').on('click', function(e){
            e.preventDefault();

            var
                $this = $(this),
                content = $('.mfilter-content ul');

            if(!$this.hasClass('trigger')){
                $this.addClass('trigger');
                $this.html('Скрыть <i class="fa fa-angle-up" aria-hidden="true">');
                content.addClass('open');
            } else {
                $this.removeClass('trigger');
                $this.html('Показать все <i class="fa fa-angle-down" aria-hidden="true">');
                content.removeClass('open');
            }
        });
    } else {
        $('.mfilter-openall').hide();
    }


    $('input[type=tel], input[name="phone"], #customer_telephone, input[name="telephone"]').mask('(999) 999-99-99');

    $('select.form-control').select2({
        minimumResultsForSearch: Infinity
    });

    $("input[name=quantity]").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });
    $('.js-height').matchHeight({
        property: 'height'
    });
    $('.news-block .description').dotdotdot({
        height: null
    });

    $('.wrapper_big-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        infinite: true,
        dots: false,
        prevArrow: '<button type="button" class="products_row-prev_btn products_row-btn"><i class="icon-left-arrow" aria-hidden="true"></i></button>',
        nextArrow: '<button type="button" class="products_row-next_btn products_row-btn"><i class="icon-left-arrow" aria-hidden="true"></i></button>',
        initialSlide: 0
    });

    $(".wrapper_big-slider").on('init', function(event, slick) {
        $(".wrapper_big-slider").css("opacity", "1");
    });

    $('.viewed-list').slick({
        slidesToShow: 5,
        initialSlide: 0,
        slidesToScroll: 1,
        arrows: true,
        centerMode: true,
        infinite: true,
        prevArrow: '<button type="button" class="products_row-prev_btn products_row-btn"><i class="icon-right-arrow" aria-hidden="true"></i></button>',
        nextArrow: '<button type="button" class="products_row-next_btn products_row-btn"><i class="icon-right-arrow" aria-hidden="true"></i></button>',
        responsive: [
            {
                breakpoint: 1199,
                settings: {
                    infinite: true,
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 990,
                settings: {
                    infinite: true,
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    arrows: false
                }
            },
            {
                breakpoint: 768,
                settings: {
                    infinite: true,
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    arrows: false
                }

            },
            {
                breakpoint: 720,
                settings: {
                    infinite: true,
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    arrows: false
                }

            },
            {
                breakpoint: 520,
                settings: {
                    infinite: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false
                }

            }
        ]
    });

    $('.viewed-news').slick({
        slidesToShow: 4,
        initialSlide: 0,
        slidesToScroll: 1,
        arrows: true,
        centerMode: true,
        infinite: true,
        prevArrow: '<button type="button" class="products_row-prev_btn products_row-btn"><i class="icon-right-arrow" aria-hidden="true"></i></button>',
        nextArrow: '<button type="button" class="products_row-next_btn products_row-btn"><i class="icon-right-arrow" aria-hidden="true"></i></button>',
        responsive: [
            {
                breakpoint: 1199,
                settings: {
                    infinite: true,
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 990,
                settings: {
                    infinite: true,
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    arrows: false
                }
            },
            {
                breakpoint: 768,
                settings: {
                    infinite: true,
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    arrows: false
                }

            },
            {
                breakpoint: 720,
                settings: {
                    infinite: true,
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    arrows: false
                }

            },
            {
                breakpoint: 520,
                settings: {
                    infinite: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false
                }

            }
        ]
    });

    $('.galery-list').slick({
        slidesToShow: 5,
        initialSlide: 0,
        slidesToScroll: 1,
        arrows: true,
        infinite: true,
        prevArrow: '<button type="button" class="products_row-prev_btn products_row-btn"><i class="icon-right-arrow" aria-hidden="true"></i></button>',
        nextArrow: '<button type="button" class="products_row-next_btn products_row-btn"><i class="icon-right-arrow" aria-hidden="true"></i></button>',
        responsive: [
            {
                breakpoint: 1199,
                settings: {
                    infinite: true,
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 990,
                settings: {
                    infinite: true,
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    arrows: true
                }
            },
            {
                breakpoint: 768,
                settings: {
                    infinite: true,
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    arrows: true
                }

            },
            {
                breakpoint: 720,
                settings: {
                    infinite: true,
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    arrows: true
                }

            },
            {
                breakpoint: 520,
                settings: {
                    infinite: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: true
                }

            }
        ]
    });
    $('.category-slider').slick({
        slidesToShow: 5,
        initialSlide: 0,
        slidesToScroll: 1,
        arrows: false,
        infinite: true,
        prevArrow: '<button type="button" class="products_row-prev_btn products_row-btn"><i class="icon-left-arrow" aria-hidden="true"></i></button>',
        nextArrow: '<button type="button" class="products_row-next_btn products_row-btn"><i class="icon-left-arrow" aria-hidden="true"></i></button>',
        responsive: [
            {
                breakpoint: 1199,
                settings: {
                    infinite: true,
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 990,
                settings: {
                    infinite: true,
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    arrows: false
                }
            },
            {
                breakpoint: 768,
                settings: {
                    infinite: true,
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    arrows: false
                }

            },
            {
                breakpoint: 720,
                settings: {
                    infinite: true,
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    arrows: false
                }

            },
            {
                breakpoint: 520,
                settings: {
                    infinite: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false
                }

            }
        ]
    });

    $('.viewed-news').slick({
        slidesToShow: 3,
        initialSlide: 0,
        slidesToScroll: 1,
        arrows: true,
        infinite: true,
        dots: true,
        prevArrow: '<button type="button" class="products_row-prev_btn products_row-btn big-slider"><i class="icon-left-arrow" aria-hidden="true"></i></button>',
        nextArrow: '<button type="button" class="products_row-next_btn products_row-btn big-slider"><i class="icon-left-arrow" aria-hidden="true"></i></button>',
        responsive: [
            {
                breakpoint: 1199,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    infinite: true
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    infinite: true,
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    arrows: true
                }
            },
            {
                breakpoint: 991,
                settings: {
                    infinite: true,
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    arrows: true
                }
            },
            {
                breakpoint: 768,
                settings:{
                    infinite: true,
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    arrows: false
                }
            }
        ]
    });

    $('input, textarea').focus(function () {
        $(this).closest('.has-error').find('.simplecheckout-error-text').remove();
        $(this).closest('.has-error').find('.text-danger').remove();
        $(this).closest('.has-error').removeClass('has-error');
    });

    $('input, textarea').focus(function () {
        $(this).closest('form').find('.alert-danger').remove();
    });

    $('.form-style input').focus(function() {
        $(this).closest('.form-group ').find('.simplecheckout-rule-group').remove();
        $(this).closest('.form-group ').removeClass('has-error');
        $(this).closest('.form-group ').find('.error').remove();
        $(this).closest('.form-group ').find('.text-danger').remove();
    });
    $('.form-group input').focus(function() {
        $(this).closest('.form-group ').find('.simplecheckout-rule-group').remove();
        $(this).closest('.form-group ').removeClass('has-error ');
        $(this).closest('.form-group ').find('.error').remove();
        $(this).closest('.form-group ').find('.text-danger').remove();
    });

    $('.form-group textarea').focus(function() {
        $(this).closest('.form-group ').find('.simplecheckout-rule-group').remove();
        $(this).closest('.form-group ').removeClass('has-error ');
        $(this).closest('.form-group ').find('.error').remove();
        $(this).closest('.form-group ').find('.text-danger').remove();
    });

});
$(document).mouseup(function (e) {
    var container = $(".colorSelector"),
        container2 = $(".colorSelector2");

    if (container.has(e.target).length === 0){
        container.hide();
    }
    if (container2.has(e.target).length === 0){
        container2.hide();
    }
});
