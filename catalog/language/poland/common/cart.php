<?php
$_['text_items'] = '%s';
$_['text_empty'] = 'Wózek jest pusty!';
$_['text_cart'] = 'Otwórz Kosz';
$_['text_cart2'] = 'Twój koszyk';
$_['text_checkout'] = 'Checkout';
$_['text_recurring'] = 'Profil płatności';

$_['cart_text1'] = 'Twój koszyk jest pusty';
$_['cart_text2'] = 'Zobacz koszyk';