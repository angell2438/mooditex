<?php
// Text
$_['text_information']  = 'Informacje';
$_['text_service']      = 'Biuro Obsługi Klienta';
$_['text_extra']        = 'Dodatki';
$_['text_contact']      = 'Kontakt z nami';
$_['text_return']       = 'Zwroty';
$_['text_sitemap']      = 'Mapa strony';
$_['text_manufacturer'] = 'Producenci';
$_['text_voucher']      = 'Bon upominkowy';
$_['text_affiliate']    = 'Partnerzy';
$_['text_special']      = 'Promocje';
$_['text_account']      = 'Moje konto';
$_['text_order']        = 'Historia zamówień';
$_['text_wishlist']     = 'Lista życzeń';
$_['text_newsletter']   = 'Subskrybuj nowości';
$_['text_powered']      = '© 2017 wooditex.com';
$_['text_email']     = 'Twój e-mail';
$_['entry_name']      	= 'Imię';
$_['entry_phone']       = 'Numer telefonu';
$_['text_call']      	= 'Zamów połączenie';
$_['text_send']      	= 'Wyślij';
$_['text_loading']      = 'Trening';
$_['text_call_email']   = 'Napisz wiadomość';