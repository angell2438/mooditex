<?php
// tekst
$_['text_home'] = 'Strona główna';
$_['text_wishlist'] = '%s towarów';
$_['text_shopping_cart'] = 'Zamów koszyk';
$_['text_category'] = 'Kategorie towarów';
$_['text_account'] = 'Gabinet osobisty';
$_['text_register'] = 'Zarejestruj';
$_['text_login'] = 'Autoryzacja';
$_['text_order'] = 'Historia zamówień';
$_['text_transaction'] = 'Katalogii';
$_['text_download'] = 'Galeria';
$_['news_name']      = 'Wiadomości';
$_['text_logout'] = 'Wyjście';
$_['text_checkout'] = 'Zamawianie';
$_['text_search'] = 'Szukaj';
$_['text_all'] = 'Pokaż wszystkie';
$_['text_page'] = 'Strona';
$_['text_gallery'] = 'Galeria';

$_['text_shop'] = 'Sklep internetowy';
$_['text_sot'] = 'Współpraca';
$_['text_company'] = 'O firmie';
$_['text_contact'] = 'Kontakty';
$_['total_wishlist'] = '%s towarów';
$_['text_city1']          = 'Shepetivka';
$_['text_city2']          = 'Chmielnicki';