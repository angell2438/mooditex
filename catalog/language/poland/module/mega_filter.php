<?php
$_['heading_title'] = 'Filter';
$_['name_price'] = 'Cena';
$_['name_manufacturers'] = 'Producent';
$_['name_rating'] = 'Ocena';
$_['name_search'] = 'Szukaj';
$_['name_stock_status'] = 'Statut w magazynie';
$_['name_location'] = 'Materiał';
$_['name_length'] = 'Length';
$_['name_width'] = 'Szerokość';
$_['name_height'] = 'Wysokość';
$_['name_mpn'] = 'Pojemność w pudełku';
$_['name_isbn'] = 'Kolor';
$_['name_sku'] = 'Artykuł';
$_['name_upc'] = 'Cena dotyczy 1';
$_['name_ean'] = 'Rozmiar';
$_['name_jan'] = 'Typ powierzchni';
$_['name_model'] = 'Model';
$_['text_button_apply'] = 'Zastosuj';
$_['text_reset_all'] = 'Resetuj wszystko';
$_['text_show_more'] = 'Pokaż w spokoju (%s)';
$_['text_show_less'] = 'Ukryj';
$_['text_display'] = 'Pokaż';
$_['text_grid'] = 'Grid';
$_['text_list'] = 'Arkusz';
$_['text_loading'] = 'Ładowanie ...';
$_['text_select'] = 'Wybór ...';
$_['text_go_to_top'] = 'Podnieś do góry';
$_['text_init_filter'] = 'Inicjalizacja filtra';
$_['text_initializing'] = 'Inicjalizacja ...';
?>

