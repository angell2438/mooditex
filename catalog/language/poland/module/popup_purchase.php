<?php
// Nagłówek
$_['heading_title'] = 'Kupuj po jednym kliknięciu';

// Przycisk
$_['button_shopping'] = 'Kontynuuj zakupy';
$_['button_checkout'] = 'Złóż zamówienie';
$_['button_upload'] = 'Pobierz zamówienie';
$_['button_purchase_now'] = 'Kupuj po jednym kliknięciu';

// tekst
$_['text_price'] = 'Cena:';
$_['text_reward'] = 'Punkty bonusowe:';
$_['text_points'] = 'Jest uchetom punktacji z:';
$_['text_tax'] = 'Brak podatków:';
$_['text_discount'] = 'lub';
$_['text_option'] = 'Dostępne opcje';
$_['text_minimum'] = 'Minimalna ilość zamówienia %s';
$_['text_select'] = '--- Proszę dokonać wyboru ---';
$_['text_success_order'] = '<p> Twoje zamówienie powodzeniem ramce. </p><p> Nasz kierownik svyazhetsya z Państwem wkrótce. </p><p> Dzięki za zakupy w sklepie internetowym nashem</p> ';
$_['text_loading'] = 'Ładowanie ...';

// Enter
$_['enter_firstname'] = 'Wpisz IM';
$_['enter_telephone'] = 'Wprowadź numer telefonu';
$_['enter_email'] = 'Wpisz adres e-mail';
$_['enter_comment'] = 'Wprowadź komentarz';
$_['entry_quantity'] = 'Wprowadź numer:';


// Błąd
$_['error_firstname'] = 'Najpierw \' Muszę być od 1 do 32 znaków!';
$_['error_lastname']          = 'Pole powinno mieć od 3 do 32 znaków!';
$_['error_email'] = 'Podany adres e-mail jest nieprawidłowy!';
$_['error_telephone'] = 'numer telefonu powinien być od 3 do 32 znaków!';
$_['error_comment'] = 'Komentarze powinny być od 3 do 500 znaków!';
$_['error_option'] = '%s jest obowiązkowe!';