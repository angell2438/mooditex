<?php
$_['heading_title'] = 'Moje zakładki';

// tekst
$_['text_account'] = 'Gabinet osobisty';
$_['text_instock'] = 'Dostępne';
$_['text_wishlist'] = '%s produkty';
$_['text_login'] = 'Musisz się zalogować na <a href="%s"> Konto osobiste </a> lub <a href="%s"> utworzyć konto </a>, aby dodać produkt <a Href = "%s">%s </a> w <a href="%s"> zakładkach </a>! ';
$_['text_success'] = '<a href="%s">%s </a> został dodany do <a href="%s"> zakładek </a>!';
$_['text_remove'] = 'Lista zakładek została pomyślnie zaktualizowana!';
$_['text_empty'] = 'Twoje zakładki są puste';
$_['text_heading'] = 'Moje zakładki';


// kolumna
$_['column_image'] = 'Obrazki';
$_['column_name'] = 'Nazwa produktu';
$_['column_model'] = 'Model';
$_['column_stock'] = 'W magazynie';
$_['column_price'] = 'Cena';
$_['column_action'] = 'Działanie';