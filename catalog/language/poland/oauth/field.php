<?php

///// Block head

$_['head_title'] = 'Wprowadź dodatkowe dane';

///// Błąd bloku
$_['error_name'] = 'Nazwa musi zawierać od 1 do 32 znaków!';
$_['error_lastname'] = 'Nazwisko musi zawierać od 1 do 32 znaków!';
$_['error_email'] = 'Adres email jest nieprawidłowy!';
$_['error_telephone'] = 'Telefon musi zawierać od 3 do 32 znaków!';
$_['error_password'] = 'Hasło musi zawierać od 6 do 32 znaków!';
$_['error_telephone'] = 'Sprawdź swój telefon';
$_['error_exists'] = 'Ostrzeżenie: adres e-mail jest już zarejestrowany!';
$_['error_agree'] = 'Ostrzeżenie: musisz zgodzić się z %s!';
$_['error_warning'] = 'Ostrzeżenie! Proszę dokładnie sprawdzić formularz pod kątem błędów!';
$_['error_approved'] = 'Ostrzeżenie: Twoje konto wymaga potwierdzenia przed zalogowaniem.';
$_['error_login'] = 'Ostrzeżenie: nie ma dopasowania do adresu e-mail i / lub hasła.';

///// Zablokuj wejście

$_['input_name'] = 'Name';
$_['input_lastname'] = 'Nazwa';
$_['input_email'] = 'E-mail';
$_['input_telephone'] = 'Telefon';
$_['input_password'] = 'Hasło';

///// Przycisk Zablokuj

$_['buttom_back'] = 'Wstecz';
$_['button_continue'] = 'Kontynuuj';