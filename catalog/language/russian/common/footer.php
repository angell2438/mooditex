<?php
// Text
$_['text_information']  = 'Wooditex';
$_['text_service']      = 'Товары и услуги';
$_['text_extra']        = 'Дополнительно';
$_['text_contact']      = 'Контакты';
$_['text_return']       = 'Возврат товара';
$_['text_sitemap']      = 'Карта сайта';
$_['text_manufacturer'] = 'Производители';
$_['text_voucher']      = 'Подарочные сертификаты';
$_['text_affiliate']    = 'Партнёры';
$_['text_special']      = 'Товары со скидкой';
$_['text_account']      = 'Личный Кабинет';
$_['text_order']        = 'История заказов';
$_['text_wishlist']     = 'Мои Закладки';
$_['text_newsletter']   = 'Рассылка новостей';
$_['text_powered']      = '© 2017 wooditex.com';

$_['entry_name']      	= 'Имя';
$_['entry_phone']       = 'Номер мобильного телефона';
$_['text_call']      	= 'Заказать звонок';
$_['text_call_email']   = 'написать сообщение';
$_['text_send']      	= 'Отправить';
$_['text_loading']      = 'Обработка';
$_['text_email']     = 'Ваш e-mail';