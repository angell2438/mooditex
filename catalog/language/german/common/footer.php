<?php
/**
 * @version		$Id: footer.php 4343 2016-06-01 10:18:23Z mic $
 * @package		Translation Deutsch
 * @author		mic - http://osworx.net
 * @copyright	2014 OSWorX - http://osworx.net
 * @license		GPL - www.gnu.org/copyleft/gpl.html
 */

// Text
$_['text_information']	= 'Informationen';
$_['text_service']		= 'Kundendienst';
$_['text_extra']		= 'Extras';
$_['text_contact']		= 'Kontakt';
$_['text_return']		= 'Retouren';
$_['text_sitemap']		= 'Seitenübersicht';
$_['text_manufacturer']	= 'Hersteller';
$_['text_voucher']		= 'Geschenkgutscheine';
$_['text_affiliate']	= 'Partner';
$_['text_special']		= 'Angebote';
$_['text_account']		= 'Konto';
$_['text_order']		= 'Auftragsverlauf';
$_['text_wishlist']		= 'Wunschliste';
$_['text_newsletter']	= 'Newsletter';
$_['text_powered']      = '© 2017 wooditex.com';

$_['entry_name']      	= 'Name';
$_['entry_phone']       = 'Handynummer';
$_['text_call']      	= 'Bestellen Sie einen Anruf';
$_['text_send']      	= 'Senden';
$_['text_loading']      = 'Training';
$_['text_call_email']   = 'eine Nachricht schreiben';
$_['text_email']     = 'Ihre e-mail';