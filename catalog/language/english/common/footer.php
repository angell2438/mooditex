<?php
// Text
$_['text_information']  = 'Wooditex';
$_['text_service']      = 'Goods and services';
$_['text_extra']        = 'Extras';
$_['text_contact']      = 'Contact';
$_['text_return']       = 'Returns';
$_['text_sitemap']      = 'Site Map';
$_['text_manufacturer'] = 'Brands';
$_['text_voucher']      = 'Gift Vouchers';
$_['text_affiliate']    = 'Affiliates';
$_['text_special']      = 'Specials';
$_['text_account']      = 'My Account';
$_['text_order']        = 'Order History';
$_['text_wishlist']     = 'Wish List';
$_['text_newsletter']   = 'Newsletter';
$_['text_powered']      = '© 2017 wooditex.com';

$_['entry_name']      	= 'Name';
$_['entry_phone']       = 'Phone:';
$_['text_call']      	= 'Order a call';
$_['text_send']      	= 'Send';
$_['text_loading']      = 'Workout';
$_['text_call_email']   = 'Write a message';
$_['text_email']     = 'Your e-mail';