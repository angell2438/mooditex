<?php
// Heading
$_['heading_title']    = 'Last Viewed';

// Text
$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: you modified the "Last Viewed" module!';
$_['text_edit']        = 'Edit Last Viewed Module';

// Entry
$_['entry_name']       = 'Module Name';
$_['entry_limit']      = 'Limit';
$_['entry_width']      = 'Width';
$_['entry_height']     = 'Height';
$_['entry_status']     = 'Status';

// Error
$_['error_permission']  = 'Warning: you don\'t have permission to modify this module!';
$_['error_width']       = 'Width needed!';
$_['error_height']      = 'Height needed!';